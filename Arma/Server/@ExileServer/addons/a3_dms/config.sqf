/*
	Main DMS Config File

	Created by eraser1
	Several revisions and additions have been made by community members.


	A lot of these configs are influenced by WAI :P
	https://github.com/nerdalertdk/WICKED-AI
*/

// You dawg... heard you like configs... so here's some configs for your config.... so you can configure your configuration to make it easier to configure your configuration http://i.imgur.com/9eJjEEo.jpg


// If you don't want the AI to have marksman DLC weapons, then simply remove the line below, or comment it by putting // at the beginning of the line
#define GIVE_AI_MARKSMAN_DLC_WEAPONS 1

// If you don't want crates to spawn with marksman DLC weapons, simply remove the line below or comment it out.
#define USE_MARKSMAN_DLC_WEAPONS_IN_CRATES 1

// Uncomment this if you want Apex weapons on AI.
//#define GIVE_AI_APEX_WEAPONS 1

// Uncomment this if you want Apex gear on AI. Uniforms, Vests, Backpacks, Helmets,Scopes
//#define GIVE_AI_APEX_GEAR 1

// Uncomment this if you want Apex weapons in loot crates
//#define USE_APEX_WEAPONS_IN_CRATES 1

// Uncomment this if you want Apex vehicles to spawn for AI/missions
//#define USE_APEX_VEHICLES 1




DMS_Use_Map_Config = false;	// Whether or not to use config overwrites specific to the map.
/*
	If you are using a map other than a map listed in the "map_configs" folder, you should set this to false OR create a new file within the map_configs folder for the map so that you don't get a missing file error.
	To share your map-specific config, please create a merge request on GitHub and/or leave a message on the DMS thread in the Exile forums.
	For any questions regarding map-specific configs, please leave a reply in the DMS thread on the Exile forums.
*/

DMS_Enable_RankChange = false; // Whether or not to use Rank Changes. (Required 'true' if using Occupation)
/*
	I am sharing this upgrade to all. If you utilize GR8 Humanity (fully compatible) or a custom version of a ranking system(simple variable changes), this will allow your players to score +/- for Bandit and Hero kills as well as a custom Survivor Faction added to DMS as well. You can still utilize the HERO / BANDIT / SURVIVOR respect and poptab settings for gameplay :) ENJOY! DONKEYPUNCH.INFO!
*/

DMS_Add_AIKill2DB = false;  // Adds killstat for player in the database ;)

DMS_SpawnMissions_Scheduled = false;	// Whether or not to spawn missions in a scheduled environment. Setting to true may help with lag when certain missions spawn.
//Note, if you have the above to true, you need to set DMS_ai_freezeOnSpawn = false; and DMS_ai_share_info = true;

/* Mission System Settings */
	/*General settings for dynamic missions*/
	DMS_DynamicMission					= true;						// Enable/disable dynamic mission system.
	DMS_MaxBanditMissions				= 10;						// Maximum number of Bandit Missions running at the same time
	DMS_TimeToFirstMission				= [180,420];				// [Minimum,Maximum] time between first mission spawn. | DEFAULT: 3-7 minutes.
	DMS_TimeBetweenMissions				= [600,900];				// [Minimum,Maximum] time between missions (if mission limit is not reached) | DEFAULT: 10-15 mins
	DMS_MissionTimeout					= [900,1800]; 				// [Minimum,Maximum] time it will take for a mission to timeout | DEFAULT: 15-30 mins
	DMS_MissionTimeoutResetRange		= 1500;						// If a player is this close to a mission then it won't time-out. Set to 0 to disable this check.
	DMS_MissionTimeoutResetFrequency	= 180;						// How often (in seconds) to check for nearby players and reset the mission timeout.
	DMS_ResetMissionTimeoutOnKill		= true;						// Whether or not to reset the mission timeout when an AI is killed.
	/*General settings for dynamic missions*/

	/*General settings for static missions*/
	DMS_StaticMission					= false;						// Enable/disable static mission system.
	DMS_MaxStaticMissions				= 1;						// Maximum number of Static Missions running at the same time. It's recommended you set this to the same amount of static missions that you have in total. This config will be ignored by "DMS_StaticMissionsOnServerStart".
	DMS_TimeToFirstStaticMission		= [30,30];					// [Minimum,Maximum] time between first static mission spawn. | DEFAULT: 3-7 minutes.
	DMS_TimeBetweenStaticMissions		= [900,1800];				// [Minimum,Maximum] time between static missions (if static mission limit is not reached) | DEFAULT: 15-30 mins
	DMS_StaticMissionTimeOut			= [1800,3600]; 				// [Minimum,Maximum] time it will take for a static mission to timeout | DEFAULT: 30-60 mins
	DMS_StaticMissionTimeoutResetRange	= 1500;						// If a player is this close to a mission then it won't time-out. Set to 0 to disable this check.
	DMS_SMissionTimeoutResetFrequency	= 180;						// How often (in seconds) to check for nearby players and reset the mission timeout for static missions.
	DMS_ResetStaticMissionTimeoutOnKill	= true;						// Whether or not to reset the mission timeout when an AI is killed (for Static Missions).
	DMS_StaticMinPlayerDistance			= 1500;						// If a player is this close to a mission location, then it won't spawn the mission and will wait 60 seconds before attempting to spawn it.
	DMS_AllowStaticReinforcements		= true;						// Whether or not static missions will receive reinforcements. This will simply disable the calling of GroupReinforcementsMonitor;
	DMS_SpawnFlareOnReinforcements		= true;						// Whether or not to spawn a flare and noise when AI reinforcements have spawned.
	/*General settings for static missions*/

	DMS_playerNearRadius				= 100;						// How close a player has to be to a mission in order to satisfy the "playerNear" mission requirement (can be customized per mission).

	DMS_AI_KillPercent					= 100;						// The percent amount of AI that need to be killed for "killPercent" mission requirement (NOT IMPLEMENTED)

	/*Mission Marker settings*/
	DMS_ShowDifficultyColorLegend		= true;						// Whether or not to show a "color legend" at the bottom left of the map that shows which color corresponds to which difficulty. I know it's not very pretty, meh.
	DMS_ShowMarkerCircle				= true;					// Whether or not to show the colored "circle" around a mission marker.
	DMS_MarkerText_ShowMissionPrefix	= true;						// Whether or not to place a prefix before the mission marker text. Enable this if your players get confused by the marker names :P
	DMS_MarkerText_MissionPrefix		= "Mission:";				// The text displayed before the mission name in the mission marker.
	DMS_MarkerText_ShowAICount			= true;						// Whether or not to display the number of remaining AI in the marker name.
	DMS_MarkerText_ShowAICount_Static	= true;						// Whether or not to display the number of remaining AI in the marker name for STATIC missions.
	DMS_MarkerText_AIName				= "AI";					// What the AI will be called in the map marker. For example, the marker text can show: "Car Dealer (3 Units remaining)"
	DMS_MarkerPosRandomization			= true;					// Randomize the position of the circle marker of a mission
	DMS_MarkerPosRandomRadius			= [25,100];					// Minimum/Maximum distance that the circle marker position will be randomized | DEFAULT: 0 meters to 200 meters
	DMS_RandomMarkerBrush				= "Solid";					// See: https://community.bistudio.com/wiki/setMarkerBrush
	DMS_MissionMarkerWinDot				= true;						// Keep the mission marker dot with a "win" message after mission is over
	DMS_MissionMarkerLoseDot			= true;						// Keep the mission marker dot with a "lose" message after mission is over
	DMS_MissionMarkerWinDot_Type		= "mil_end";				// The marker type to show when a mission is completed. Refer to: https://community.bistudio.com/wiki/cfgMarkers
	DMS_MissionMarkerLoseDot_Type		= "KIA";					// The marker type to show when a mission fails. Refer to: https://community.bistudio.com/wiki/cfgMarkers
	DMS_MissionMarkerWinDotTime			= 30;						// How many seconds the "win" mission dot will remain on the map
	DMS_MissionMarkerLoseDotTime		= 30;						// How many seconds the "lose" mission dot will remain on the map
	DMS_MissionMarkerWinDotColor		= "ColorBlue";				// The color of the "win" marker dot
	DMS_MissionMarkerLoseDotColor		= "ColorRed";				// The color of the "lose" marker dot
	/*Mission Marker settings*/

	/*Mission Cleanup settings*/
	DMS_CompletedMissionCleanup			= true;						// Cleanup mission-spawned buildings and AI bodies after some time
	DMS_CompletedMissionCleanupTime		= 3600;						// Minimum time until mission-spawned buildings and AI are cleaned up
	DMS_CleanUp_PlayerNearLimit			= 20;						// Cleanup of an object is aborted if a player is this many meters close to the object
	DMS_AIVehCleanUpTime				= 300;						// Time until a destroyed AI vehicle is cleaned up.
	/*Mission Cleanup settings*/

	/*Mission spawn location settings*/
	DMS_UsePredefinedMissionLocations	= false;					// Whether or not to use a list of pre-defined mission locations instead before attempting to find a random (valid) position. The positions will still be checked for validity. If none of the provided positions are valid, a random one will be generated.
	DMS_PredefinedMissionLocations = 	[							// List of Preset/Predefined mission locations.
											/* List of positions:
											position1: [x_1,y_1,z_1],
											position2: [x_2,y_2,z_2],
											...
											positionN: [x_N,y_N,z_N]
											*/

										];

	DMS_PredefinedMissionLocations_WEIGHTED = 	[					// List of Preset/Predefined mission locations WITH WEIGHTED CHANCES. This will NOT override "DMS_PredefinedMissionLocations", and everything from "DMS_PredefinedMissionLocations" will behave as though it has 1 weight per position.
											/* List of arrays with position and weighted chance:
											[[x_1,y_1,z_1], chance_1],
											[[x_2,y_2,z_2], chance_2],
											...
											[[x_N,y_N,z_N], chance_N]
											*/

										];
	DMS_ThrottleBlacklists				= false;						// Whether or not to "throttle" the blacklist distance parameters in DMS_fnc_FindSafePos. This will reduce the values of the minimum
																		//distances for some of the below parameters if several attempts have been made, but a suitable position was not yet found. This
																		//should help with server performance drops when spawning a mission, as DMS_fnc_findSafePos is the most resource-intensive function.
	DMS_AttemptsUntilThrottle			= 25;						// How many attempts until the parameters are throttled.
	DMS_ThrottleCoefficient				= 0.5;						// How much the parameters are throttled. The parameters are multiplied by the coefficient, so 0.9 means 90% of whatever the parameter was.
	DMS_MinThrottledDistance			= 250;						// The minimum distance to which it will throttle. If the throttled value is less than this value, then this value is used instead.
	DMS_PlayerNearBlacklist				= 1500;						// Missions won't spawn in a position this many meters close to a player
	DMS_SpawnZoneNearBlacklist			= 2000;						// Missions won't spawn in a position this many meters close to a spawn zone
	DMS_TraderZoneNearBlacklist			= 1500;						// Missions won't spawn in a position this many meters close to a trader zone
	DMS_MissionNearBlacklist			= 2500;						// Missions won't spawn in a position this many meters close to another mission
	DMS_WaterNearBlacklist				= 100;						// Missions won't spawn in a position this many meters close to water
	DMS_TerritoryNearBlacklist			= 100;						// Missions won't spawn in a position this many meters close to a territory flag. This is a resource intensive check, don't set this value too high!
	DMS_MixerNearBlacklist				= 250;						// Missions won't spawn in a position this many meters close to a concrete mixer
	DMS_ContaminatedZoneNearBlacklist	= 5;						// Missions won't spawn in a position this many meters close to a contaminated zone
	DMS_MinSurfaceNormal				= 0.4;						// Missions won't spawn in a position where its surfaceNormal is less than this amount. The lower the value, the steeper the location. Greater values means flatter locations. Values can range from 0-1, with 0 being sideways, and 1 being perfectly flat. For reference: SurfaceNormal of about 0.7 is when you are forced to walk up a surface. If you want to convert surfaceNormal to degrees, use the arc-cosine of the surfaceNormal. 0.9 is about 25 degrees. Google "(arccos 0.9) in degrees"
	DMS_MinDistFromWestBorder			= 250;						// Missions won't spawn in a position this many meters close to the western map border.
	DMS_MinDistFromEastBorder			= 250;						// Missions won't spawn in a position this many meters close to the easter map border.
	DMS_MinDistFromSouthBorder			= 250;						// Missions won't spawn in a position this many meters close to the southern map border.
	DMS_MinDistFromNorthBorder			= 250;						// Missions won't spawn in a position this many meters close to the northern map border.
	DMS_SpawnZoneMarkerTypes =			[							// If you're using custom spawn zone markers, make sure you define them here. CASE SENSITIVE!!!
											"ExileSpawnZoneIcon"
										];
	DMS_TraderZoneMarkerTypes =			[							// If you're using custom trader markers, make sure you define them here. CASE SENSITIVE!!!
											"ExileTraderZoneIcon",
											"ExileBoatTraderIcon",
											"ExileSpecOpsTraderIcon",
											"ExileAircraftTraderIcon"
										];
	DMS_MixerMarkerTypes =				[							// If you're using custom concrete mixer map markers, make sure you define them here. CASE SENSITIVE!!!
											"ExileConcreteMixerZoneIcon"
										];
	DMS_ContaminatedZoneMarkerTypes =	[							// If you're using custom contaminated zone markers, make sure you define them here. CASE SENSITIVE!!!
											"ExileContaminatedZoneIcon"
										];
	/*Mission spawn location settings*/

	DMS_MinWaterDepth					= 20;						// Minimum depth of water that an underwater mission can spawn at.

	/*Crate/Box settings*/
	DMS_HideBox							= false;					// "Hide" the box from being visible by players until the mission is completed.
	DMS_EnableBoxMoving					= true;						// Whether or not to allow the box to move and/or be lifted by choppers.
	DMS_SpawnBoxSmoke					= true;						// Spawn a smoke grenade on mission box upon misson completion during daytime
	DMS_DefaultSmokeClassname 			= "SmokeShellPurple";		// Classname of the smoke you want to spawn.
	DMS_SpawnBoxIRGrenade				= true;						// Spawn an IR grenade on mission box upon misson completion during nighttime
	/*Crate/Box settings*/

	/*Mine settings*/
	DMS_SpawnMinefieldForEveryMission	= false;					// Whether or not to spawn a minefield for every dynamic mission.
	DMS_SpawnMinesAroundMissions		= true;						// Whether or not to spawn mines around AI missions that have them.
	DMS_despawnMines_onCompletion		= true;						// Despawn mines spawned around missions when the mission is completed
	DMS_MineInfo_easy					= [5,50];					// Mine info for "easy" missions. This will spawn 5 mines within a 50m radius.
	DMS_MineInfo_moderate				= [10,50];					// Mine info for "moderate" missions. This will spawn 10 mines within a 50m radius.
	DMS_MineInfo_difficult				= [15,75];					// Mine info for "difficult" missions. This will spawn 15 mines within a 75m radius.
	DMS_MineInfo_hardcore				= [25,100];					// Mine info for "hardcore" missions. This will spawn 25 mines within a 100m radius.
	DMS_SpawnMineWarningSigns			= true;						// Whether or not to spawn mine warning signs around a minefield.
	DMS_BulletProofMines				= true;						// Whether or not you want to make the mines bulletproof. Prevents players from being able to shoot the mines and creating explosions.
	/*Mine settings*/

	DMS_MinPlayerCount					= 0; 						// Minimum number of players until mission start
	DMS_MinServerFPS					= 15; 						// Minimum server FPS for missions to start

	/*Mission notification settings*/
	DMS_PlayerNotificationTypes =		[									// Notification types. Supported values are: ["dynamicTextRequest", "standardHintRequest", "systemChatRequest", "textTilesRequest", "ExileToasts"]. Details below.
											//"dynamicTextRequest",			// You should use either "dynamicTextRequest" or "textTilesRequest", and I think "textTilesRequest" looks better, but this is less performance-intensive.
											//"standardHintRequest",		// Hints are a bit wonky...
											//"textTilesRequest",			// Keep in mind you can only have 1 "text tile" message up at a time, so the message will disappear if the player gets a kill or something while the message is shown. This message type is also performance-intensive, so I advise against it.
											//"systemChatRequest",			// Always nice to show in chat so that players can scroll up to read the info if they need to.
											"ExileToasts"					// Default notification type since Exile 0.98, see (http://www.exilemod.com/devblog/new-ingame-notifications/)
										];

		/*Exile Toasts Notification Settings*/
	DMS_ExileToasts_Title_Size			= 22;						// Size for Client Exile Toasts  mission titles.
	DMS_ExileToasts_Title_Font			= "puristaMedium";			// Font for Client Exile Toasts  mission titles.
	DMS_ExileToasts_Message_Color		= "#FFFFFF";				// Exile Toasts color for "ExileToast" client notification type.
	DMS_ExileToasts_Message_Size		= 19;						// Exile Toasts size for "ExileToast" client notification type.
	DMS_ExileToasts_Message_Font		= "PuristaLight";			// Exile Toasts font for "ExileToast" client notification type.
		/*Exile Toasts Notification Settings*/

		/*Dynamic Text Notification Settings*/
	DMS_dynamicText_Duration			= 7;						// Number of seconds that the message will last on the screen.
	DMS_dynamicText_FadeTime			= 1.5;						// Number of seconds that the message will fade in/out (does not affect duration).
	DMS_dynamicText_Title_Size			= 1.2;						// Size for Client Dynamic Text mission titles.
	DMS_dynamicText_Title_Font			= "puristaMedium";			// Font for Client Dynamic Text mission titles.
	DMS_dynamicText_Message_Color		= "#FFFFFF";				// Dynamic Text color for "dynamicTextRequest" client notification type.
	DMS_dynamicText_Message_Size		= 0.65;						// Dynamic Text size for "dynamicTextRequest" client notification type.
	DMS_dynamicText_Message_Font		= "OrbitronMedium";			// Dynamic Text font for "dynamicTextRequest" client notification type.
		/*Dynamic Text Notification Settings*/

		/*Standard Hint Notification Settings*/
	DMS_standardHint_Title_Size			= 2;						// Size for Client Standard Hint mission titles.
	DMS_standardHint_Title_Font			= "puristaMedium";			// Font for Client Standard Hint mission titles.
	DMS_standardHint_Message_Color		= "#FFFFFF";				// Standard Hint color for "standardHintRequest" client notification type.
	DMS_standardHint_Message_Size		= 1;						// Standard Hint size for "standardHintRequest" client notification type.
	DMS_standardHint_Message_Font		= "OrbitronMedium";			// Standard Hint font for "standardHintRequest" client notification type.
		/*Standard Hint Notification Settings*/

		/*Text Tiles Notification Settings*/
	DMS_textTiles_Duration				= 7;						// Number of seconds that the message will last on the screen.
	DMS_textTiles_FadeTime				= 1.5;						// Number of seconds that the message will fade in/out (does not affect duration).
	DMS_textTiles_Title_Size			= 2.3;						// Size for Client Text Tiles mission titles.
	DMS_textTiles_Title_Font			= "puristaMedium";			// Font for Client Text Tiles mission titles.
	DMS_textTiles_Message_Color			= "#FFFFFF";				// Text Tiles color for "textTilesRequest" client notification type.
	DMS_textTiles_Message_Size			= 1.25;						// Text Tiles size for "textTilesRequest" client notification type.
	DMS_textTiles_Message_Font			= "OrbitronMedium";			// Text Tiles font for "textTilesRequest" client notification type.
		/*Text Tiles Notification Settings*/

	/*Mission notification settings*/

	DMS_RandomBanditMissionsOnStart		= 6;						// Number of (random) bandit missions to spawn when the server starts, just so players don't have to wait for missions to spawn.
	DMS_BanditMissionTypes =			[			//	List of missions with spawn chances. If they add up to 100%, they represent the percentage chance each one will spawn
											["bandits",3],
											["bauhaus",3],
											["beertransport",3],
											["behindenemylines",3],
											["blackhawkdown",3],
											["cardealer",3],
											["construction",3],
											["donthasslethehoff",3],
											["foodtransport",3],
											["guntransport",3],
											["humanitarian",3],
											["lost_battalion",3],
											["medical",3],
											["mercbase",2],
											["mercenaries",3],
											["nedbuilding1_mission",3],
											["nedcar_mission",4],
											["nedguns1_mission",3],
											["nedhatchback_mission",3],
											["nedhunter_mission",2],
											["nedifrit_mission",2],
											["nedlittlebird_mission",2],
											["nedmedical1_mission",3],
											["nedoffroad_mission",3],
											["nedresearch_mission",3],
											["nedsnipercamp_mission",3],
											["nedstrider_mission",2],
											["nedural_mission",3],
											["roguenavyseals",3],
											["thieves",3],
											["walmart",3],
											["paul_fisher",5],
                                            ["paul_safe_mission",5],
                                            ["paul_industrial_new",5],
                                            ["paul_bandits",5],
                                            ["paul_medical",5],
                                            ["paul_dome_mission",5],
                                            ["paul_military_camp",5],
                                            ["paul_cargo_tower",5],
                                            ["paul_beer_transport",5],
                                            ["paul_strider_mission",5],
                                            ["paul_church_mission",5],
                                            ["paul_water_mission",5],
                                            ["paul_littlebird_mission",5],
                                            ["paul_boat_mission",5],
                                            ["paul_clean_energy",5],
                                            ["paul_ah1z",5],
                                            ["paul_jackal",5],
                                            ["paul_hmmwv",5],
                                            ["paul_sandbags_mission",5],
                                            ["paul_highend_mission",5],
                                            ["paul_weed_mission",5],
                                            ["paul_castle",5],
                                            ["paul_apc",5],
                                            ["paul_shootingRange",5],
                                            ["paul_train_crash",5],
                                            ["paul_c130",5],
                                            ["paul_antiair",5],
                                            ["paul_garage",5]
										];


	DMS_StaticMissionTypes =			[								// List of STATIC missions with spawn chances.
											//["saltflats",1]		//<--Example (already imported by default on Altis in map configs)
											//["slums",1]			//<--Example (already imported by default on Altis in map configs)
											//["occupation",1]		//<--Example
											//["sectorB",1]			//<--Example for Taviana
										];

	DMS_SpecialMissions =				[								// List of special missions with restrictions. Each element must be defined as [mission<STRING>, minPlayers<SCALAR>, maxPlayers<SCALAR>, timesPerRestart<SCALAR>, _timeBetween<SCALAR>].
											//["specops",15,60,2,900]	//<-- Example for a mission named "specops.sqf" that must be placed in the "special" folder. It will only spawn when there are at least 15 players, less than 60 players, it will only spawn up to twice per restart, and at least 900 seconds must pass before another instance of the mission can spawn.
										];

	DMS_BasesToImportOnServerStart = 	[								// List of static bases to import on server startup (spawned post-init). This will reduce the amount of work the server has to do when it actually spawns static missions, and players won't be surprised when a base suddenly pops up. You can also include any other M3E-exported bases to spawn here.
											//"saltflatsbase",		//<--Example (already imported by default on Altis)
											//"slums_objects"		//<--Example (already imported by default on Altis)
										];

	DMS_BanditMissionsOnServerStart =	[
											//"construction"		//<-- Example
										];

	DMS_StaticMissionsOnServerStart =	[								// List of STATIC missions with spawn chances.
											//"saltflats"			//<--Example
											//"slums"				//<--Example
											//"occupation"			//<--Example
											//"sectorB"				//<--Example for Taviana
										];



	DMS_findSafePosBlacklist =			[								// This list defines areas where missions WILL NOT spawn. For position blacklist info refer to: http://www.exilemod.com/topic/61-dms-defents-mission-system/?do=findComment&comment=31190
											// There are examples in the altis map config (it blacklists the salt flats) and in the tavi/taviana map configs.

											//[[2350,4680],100]		// This random example blacklists any position within 100 meters of coordinates "[2350,4680]"
										];
/* Mission System Settings */


/* AI Settings */
	DMS_AI_Classname					= "O_Soldier_unarmed_F";				// Since some of you wanted this...

	DMS_AI_NamingType					= 0;						// This specifies the "naming scheme" for the AI. 0 corresponds with the default ArmA names; 1 means you want a DMS name (eg: [DMS BANDIT SOLDIER 123]); 2 means you want to generate a name from a list of first and last names (DMS_AI_FirstNames, DMS_AI_LastNames).
	DMS_AI_FirstNames =					[							// List of "first names" that an AI can have. Only used when DMS_AI_NamingType = 2.
											"Adam",
											"Benjamin",
											"Charles",
											"David",
											"Eric"
											// etc.
										];
	DMS_AI_LastNames =					[							// List of "last names" that an AI can have. Only used when DMS_AI_NamingType = 2.
											"Smith",
											"Johnson",
											"Williams",
											"Jones",
											"Brown"
											// etc.
										];

	DMS_Show_Kill_Poptabs_Notification	= true;						// Whether or not to show the poptabs gained/lost message on the player's screen when killing an AI. (It will still change the player's money, it just won't show the "Money Received" notification)
	DMS_Show_Kill_Respect_Notification	= true;						// Whether or not to show the "Frag Message" on the player's screen when killing an AI. (It will still change the player's respect, it just won't show the "AI Killed" frag message)
	DMS_Show_Kill_Rank_Notification		= true;
	DMS_Show_Party_Kill_Notification	= true;						// Whether or not to show in chat when a party member kills an AI.

	DMS_Spawn_AI_With_Money				= true;						// Whether or not to spawn AI with money that can be looted from the body.
	DMS_AIMoney_PopulationMultiplier	= 5;						// This determines how much EXTRA money an AI will have on his body. For example, setting this to 5 and having a server population of 30 means the AI will have an extra 150 poptabs on the body. Set to 0 to disable.

	DMS_GiveMoneyToPlayer_OnAIKill		= true;						// Whether or not to give money directly to players when they kill AI (old method of giving money).
	DMS_GiveRespectToPlayer_OnAIKill	= true;						// Whether or not to give respect to players when they kill AI.

	DMS_Bandit_Soldier_MoneyGain		= 50;						// The amount of Poptabs gained for killing a bandit soldier
	DMS_Bandit_Soldier_RepGain			= 10;						// The amount of Respect gained for killing a bandit soldier
	DMS_Bandit_Soldier_RankGain			= 15;
	DMS_Bandit_Soldier_SpawnMoney		= 50;						// The amount of Poptabs carried by a bandit soldier

	DMS_Bandit_Static_MoneyGain			= 75;						// The amount of Poptabs gained for killing a bandit static gunner
	DMS_Bandit_Static_RepGain			= 15;						// The amount of Respect gained for killing a bandit static gunner
	DMS_Bandit_Static_RankGain			= 30;
	DMS_Bandit_Static_SpawnMoney		= 75;						// The amount of Poptabs carried by a bandit static gunner

	DMS_Bandit_Vehicle_MoneyGain		= 100;						// The amount of Poptabs gained for killing a bandit vehicle crew member
	DMS_Bandit_Vehicle_RepGain			= 25;						// The amount of Respect gained for killing a bandit vehicle crew member
	DMS_Bandit_Vehicle_RankGain			= 50;
	DMS_Bandit_Vehicle_SpawnMoney		= 100;						// The amount of Poptabs carried by a bandit vehicle crew member

/* DonkeyPunchDMS Custom Settings for Hero AI*/
	DMS_Hero_Soldier_MoneyGain			= 100;						// The amount of Poptabs gained for killing a hero soldier
	DMS_Hero_Soldier_RepGain			= 20;						// The amount of Respect gained for killing a hero soldier
	DMS_Hero_Soldier_RankGain			= -30;
	DMS_Hero_Soldier_SpawnMoney			= 100;						// The amount of Poptabs carried by a hero soldier

	DMS_Hero_Static_MoneyGain			= 120;						// The amount of Poptabs gained for killing a hero static gunner
	DMS_Hero_Static_RepGain				= 30;						// The amount of Respect gained for killing a hero static gunner
	DMS_Hero_Static_RankGain			= -60;
	DMS_Hero_Static_SpawnMoney			= 120;						// The amount of Poptabs carried by a hero static gunner

	DMS_Hero_Vehicle_MoneyGain			= 200;						// The amount of Poptabs gained for killing a hero vehicle crew member
	DMS_Hero_Vehicle_RepGain			= 50;						// The amount of Respect gained for killing a hero vehicle crew member
	DMS_Hero_Vehicle_RankGain			= -100;
	DMS_Hero_Vehicle_SpawnMoney			= 200;						// The amount of Poptabs carried by a hero vehicle crew member
/* DonkeyPunchDMS Custom Settings for Survivor AI*/
	DMS_Survivor_Soldier_MoneyGain		= -100;						// The amount of Poptabs gained for killing a Survivor soldier
	DMS_Survivor_Soldier_RepGain		= -100;						// The amount of Respect gained for killing a Survivor soldier
	DMS_Survivor_Soldier_RankGain		= -250;
	DMS_Survivor_Soldier_SpawnMoney		= 0;						// The amount of Poptabs carried by a Survivor soldier

	DMS_Survivor_Static_MoneyGain		= -100;						// The amount of Poptabs gained for killing a Survivor static gunner
	DMS_Survivor_Static_RepGain			= -100;						// The amount of Respect gained for killing a Survivor static gunner
	DMS_Survivor_Static_RankGain		= -400;
	DMS_Survivor_Static_SpawnMoney		= 0;						// The amount of Poptabs carried by a Survivor static gunner

	DMS_Survivor_Vehicle_MoneyGain		= -500;						// The amount of Poptabs gained for killing a Survivor vehicle crew member
	DMS_Survivor_Vehicle_RepGain		= -100;						// The amount of Respect gained for killing a Survivor vehicle crew member
	DMS_Survivor_Vehicle_RankGain		= -600;
	DMS_Survivor_Vehicle_SpawnMoney		= 0;						// The amount of Poptabs carried by a Survivor vehicle crew member

	DMS_AIKill_DistanceBonusMinDistance	= 100;						// Minimum distance from the player to the AI to apply the distance bonus.
	DMS_AIKill_DistanceBonusCoefficient	= 0.05;						// If the distance from the player to the killed unit is more than "DMS_AIKill_DistanceBonusMinDistance" meters then the player gets a respect bonus equivalent to the distance multiplied by this coefficient. For example, killing an AI from 400 meters will give 100 extra respect (when the coefficient is 0.25). Set to 0 to disable the bonus. This bonus will not be applied if there isn't a regular AI kill bonus.

	DMS_Diff_RepOrTabs_on_roadkill 		= true;						// Whether or not you want to use different values for giving respect/poptabs when you run an AI over. Default values are NEGATIVE. This means player will LOSE respect or poptabs.
	DMS_Bandit_Soldier_RoadkillMoney	= -10;						// The amount of Poptabs gained/lost for running over a bandit soldier
	DMS_Bandit_Soldier_RoadkillRep		= -5;						// The amount of Respect gained/lost for running over a bandit soldier
	DMS_Bandit_Soldier_RoadkillRank		= 20;
	DMS_Bandit_Static_RoadkillMoney		= -10;						// The amount of Poptabs gained/lost for running over a bandit static gunner
	DMS_Bandit_Static_RoadkillRep		= -5;						// The amount of Respect gained/lost for running over a bandit static gunner
	DMS_Bandit_Static_RoadkillRank		= 30;
	DMS_Bandit_Vehicle_RoadkillMoney	= -10;						// The amount of Poptabs gained/lost for running over a bandit vehicle crew member
	DMS_Bandit_Vehicle_RoadkillRep		= -5;						// The amount of Respect gained/lost for running over a bandit vehicle crew member
	DMS_Bandit_Vehicle_RoadkillRank		= 50;
/* DonkeyPunchDMS Custom RoadKill Settings for Hero AI*/
	DMS_Hero_Soldier_RoadkillMoney		= 20;						// The amount of Poptabs gained/lost for running over a hero soldier
	DMS_Hero_Soldier_RoadkillRep		= 10;						// The amount of Respect gained/lost for running over a hero soldier
	DMS_Hero_Soldier_RoadkillRank		= -40;
	DMS_Hero_Static_RoadkillMoney		= 20;						// The amount of Poptabs gained/lost for running over a hero static gunner
	DMS_Hero_Static_RoadkillRep			= 10;						// The amount of Respect gained/lost for running over a hero static gunner
	DMS_Hero_Static_RoadkillRank		= -60;
	DMS_Hero_Vehicle_RoadkillMoney		= 20;						// The amount of Poptabs gained/lost for running over a hero vehicle crew member
	DMS_Hero_Vehicle_RoadkillRep		= 10;						// The amount of Respect gained/lost for running over a hero vehicle crew member
	DMS_Hero_Vehicle_RoadkillRank		= -100;
/* DonkeyPunchDMS Custom Roadkill Settings for Survivor AI*/
	DMS_Survivor_Soldier_RoadkillMoney	= -200;						// The amount of Poptabs gained/lost for running over a Survivor soldier
	DMS_Survivor_Soldier_RoadkillRep	= -200;						// The amount of Respect gained/lost for running over a Survivor soldier
	DMS_Survivor_Soldier_RoadkillRank	= -200;
	DMS_Survivor_Static_RoadkillMoney	= -200;						// The amount of Poptabs gained/lost for running over a Survivor static gunner
	DMS_Survivor_Static_RoadkillRep		= -200;						// The amount of Respect gained/lost for running over a Survivor static gunner
	DMS_Survivor_Static_RoadkillRank	= -200;
	DMS_Survivor_Vehicle_RoadkillMoney	= -500;						// The amount of Poptabs gained/lost for running over a Survivor vehicle crew member
	DMS_Survivor_Vehicle_RoadkillRep	= -100;						// The amount of Respect gained/lost for running over a Survivor vehicle crew member
	DMS_Survivor_Vehicle_RoadkillRank	= -100;

	DMS_banditSide						= EAST;						// The side (team) that AI Bandits will spawn on
/* DonkeyPunchDMS Custom Side Factions */
	DMS_heroSide						= WEST;						// The side (team) that AI Heros will spawn on
	DMS_survivorSide					= CIV;						// The side (team) that AI Survivor will spawn on

	DMS_clear_AI_body					= false;					// Clear AI body as soon as they die
	DMS_clear_AI_body_chance			= 50;						// Percentage chance that AI bodies will be cleared when they die
	DMS_ai_disable_ramming_damage 		= true;						// Disables damage due to ramming into AI. !!!NOTE: THIS WILL NOT BE RELIABLE WITH "DMS_ai_offload_to_client"!!!
	DMS_remove_roadkill					= true; 					// Remove gear from AI bodies that are roadkilled
	DMS_remove_roadkill_chance			= 100;						// Percentage chance that roadkilled AI bodies will be deleted
	DMS_explode_onRoadkill				= false;						// Whether or not to spawn an explosion when an AI gets run over. It will likely take out the 2 front wheels. Should help mitigate the ineffective AI vs. striders issue ;)
	DMS_RemoveNVG						= false;					// Remove NVGs from AI bodies

	DMS_MaxAIDistance					= 500;						// The maximum distance an AI unit can be from a mission before he is killed. Helps with AI running away and forcing the mission to keep running. Set to 0 if you don't want it.
	DMS_AIDistanceCheckFrequency		= 60;						// How often to check within DMS_fnc_TargetsKilled whether or not the AI is out of the maximum radius. Lower values increase frequency and increase server load, greater values decrease frequency and may cause longer delays for "runaway" AI.

	DMS_ai_offload_to_client			= true;						// Offload spawned AI groups to random clients. Helps with server performance.
	DMS_ai_offload_Only_DMS_AI			= true;						// Don't set this to false unless you know what you're doing.
	DMS_ai_offload_notifyClient			= false;					// Notify the client when AI has been offloaded to the client.

	DMS_ai_allowFreezing				= true;						// Whether or not to "freeze" AI that are a certain distance away from players (and therefore inactive).
	DMS_ai_freeze_Only_DMS_AI			= false;					// Whether or not to "freeze" AI that are not spawned by DMS.
	DMS_ai_freezingDistance				= 3500;						// If there are no players within this distance of the leader of an AI group, then the AI group will be "frozen".
	DMS_ai_unfreezingDistance			= 3500;						// If there are players within this distance of the leader of an AI group, then the AI group will be "un-frozen".
	DMS_ai_offloadOnUnfreeze			= true;						// Whether or not to offload AI to clients once they have been "un-frozen". NOTE: This config will be ignored if "DMS_ai_offload_to_client" is set to false.
	DMS_ai_freezeCheckingDelay			= 15;						// How often (in seconds) DMS will check whether to freeze/un-freeze AI.
	DMS_ai_freezeOnSpawn				= true;						// Whether or not to freeze an AI group when initially spawned.

	DMS_ai_share_info					= false;					// Share info about killer
	DMS_ai_share_info_distance			= 25;						// The distance killer's info will be shared to other AI

	DMS_ai_nighttime_accessory_chance	= 75;						// Percentage chance that AI will have a flashlight or laser pointer on their guns if spawned during nighttime
	DMS_ai_enable_water_equipment		= true;						// Enable/disable overriding default weapons of an AI if it spawns on/in water

	// https://community.bistudio.com/wiki/AI_Sub-skills#general
	DMS_ai_skill_static					= [["aimingAccuracy",0.20],["aimingShake",0.70],["aimingSpeed",0.75],["spotDistance",0.70],["spotTime",0.50],["courage",1.00],["reloadSpeed",1.00],["commanding",1.00],["general",1.00]];	// Static AI Skills
	DMS_ai_skill_easy					= [["aimingAccuracy",0.30],["aimingShake",0.50],["aimingSpeed",0.50],["spotDistance",0.50],["spotTime",0.50],["courage",1.00],["reloadSpeed",1.00],["commanding",1.00],["general",0.50]];	// Easy
	DMS_ai_skill_moderate				= [["aimingAccuracy",0.60],["aimingShake",0.60],["aimingSpeed",0.60],["spotDistance",0.60],["spotTime",0.60],["courage",1.00],["reloadSpeed",1.00],["commanding",1.00],["general",0.60]];	// Moderate
	DMS_ai_skill_difficult				= [["aimingAccuracy",0.70],["aimingShake",0.70],["aimingSpeed",0.70],["spotDistance",0.70],["spotTime",0.80],["courage",1.00],["reloadSpeed",1.00],["commanding",1.00],["general",0.70]]; 	// Difficult
	DMS_ai_skill_hardcore				= [["aimingAccuracy",1.00],["aimingShake",1.00],["aimingSpeed",1.00],["spotDistance",1.00],["spotTime",1.00],["courage",1.00],["reloadSpeed",1.00],["commanding",1.00],["general",1.00]]; 	// Hardcore
	DMS_ai_skill_random					= ["hardcore","difficult","difficult","difficult","moderate","moderate","moderate","moderate","easy","easy"];	// Skill frequencies for "random" AI skills | Default: 10% hardcore, 30% difficult, 40% moderate, and 20% easy
	DMS_ai_skill_randomDifficult		= ["hardcore","hardcore","difficult","difficult","difficult"];	// 60% chance for "difficult", 40% chance for "hardcore" AI.
	DMS_ai_skill_randomEasy				= ["moderate","moderate","easy","easy","easy"];					// 60% chance for "easy", 40% chance for "moderate" AI.
	DMS_ai_skill_randomIntermediate		= ["difficult","difficult","moderate","moderate","moderate"];	// 60% chance for "moderate", 40% chance for "difficult" AI.
	DMS_AI_WP_Radius_easy				= 20;						// Waypoint radius for "easy" AI.
	DMS_AI_WP_Radius_moderate			= 30;						// Waypoint radius for "moderate" AI.
	DMS_AI_WP_Radius_difficult			= 50;						// Waypoint radius for "difficult" AI.
	DMS_AI_WP_Radius_hardcore			= 75;						// Waypoint radius for "hardcore" AI.
	DMS_AI_AimCoef_easy					= 0.9;						// "Custom Aim Coefficient" (weapon sway multiplier) for "easy" AI
	DMS_AI_AimCoef_moderate				= 0.65;						// "Custom Aim Coefficient" (weapon sway multiplier) for "moderate" AI
	DMS_AI_AimCoef_difficult			= 0.4;						// "Custom Aim Coefficient" (weapon sway multiplier) for "difficult" AI
	DMS_AI_AimCoef_hardcore				= 0.05;						// "Custom Aim Coefficient" (weapon sway multiplier) for "hardcore" AI
	DMS_AI_EnableStamina_easy			= true;						// Whether or not to keep the stamina system for "easy" AI.
	DMS_AI_EnableStamina_moderate		= true;						// Whether or not to keep the stamina system for "moderate" AI.
	DMS_AI_EnableStamina_difficult		= false;					// Whether or not to keep the stamina system for "difficult" AI.
	DMS_AI_EnableStamina_hardcore		= false;					// Whether or not to keep the stamina system for "hardcore" AI.
	DMS_AI_WP_Radius_base				= 5;						// Waypoint radius for AI in bases.
	DMS_AI_WP_Radius_heli				= 1500;						// Waypoint radius for AI in helis.

	DMS_AI_destroyVehicleChance			= 0;						// Percent chance that an AI vehicle will be destroyed after the AI have been killed. Set to 100 for always, or 0 for never.

	DMS_AI_destroyStaticWeapon			= true;						// Whether or not to destroy static HMGs after AI death.
	DMS_AI_destroyStaticWeapon_chance	= 50;						// Percent chance that a static weapon will be destroyed (only applied if "DMS_AI_destroyStaticWeapon" is true)

DMS_static_weapons =				[							// Static weapons for AI
											"O_HMG_01_high_F"
										];

	DMS_ai_default_items =				[							// Toolbelt items each AI will spawn with
											"ItemWatch",
											"ItemMap",
											"ItemCompass",
											"ItemRadio"
										];

	DMS_ai_BipodList =					[
											"bipod_01_F_blk",
											"bipod_01_F_mtp",
											"bipod_01_F_snd",
											"bipod_02_F_blk",
											"bipod_02_F_hex",
											"bipod_02_F_tan",
											"bipod_03_F_blk",
											"bipod_03_F_oli"
										];

	//Assault Class
	DMS_assault_weps =					[							// Assault Rifles
											#ifdef GIVE_AI_APEX_WEAPONS
											"arifle_AK12_F",
											"arifle_ARX_ghex_F",
											"arifle_CTAR_blk_F",
											"arifle_SPAR_01_khk_F",
											"arifle_SPAR_03_khk_F",
											#endif
											"arifle_Katiba_GL_F",
											"arifle_MX_GL_Black_F",
											"arifle_Mk20_GL_F",
											"arifle_TRG21_GL_F",
											"arifle_Katiba_F",
											"arifle_MX_Black_F",
											"arifle_TRG21_F",
											"arifle_TRG20_F",
											"arifle_Mk20_plain_F",
											"arifle_Mk20_F",
											"Exile_Weapon_AK107",
											"Exile_Weapon_AK107_GL",
											"Exile_Weapon_AK74_GL",
											"Exile_Weapon_AK47",
											"Exile_Weapon_AKS_Gold",
											"CUP_srifle_AWM_des",
											"CUP_srifle_AWM_wdl",
											"CUP_srifle_CZ750",
											"CUP_srifle_DMR",
											"CUP_srifle_CZ550",
											"CUP_srifle_LeeEnfield",
											"CUP_srifle_M14",
											"CUP_srifle_Mk12SPR",
											"CUP_srifle_M24_des",
											"CUP_srifle_M24_wdl",
											"CUP_srifle_M40A3",
											"CUP_srifle_M107_Base",
											"CUP_srifle_M110",
											"CUP_srifle_SVD",
											"CUP_srifle_SVD_des",
											"CUP_srifle_ksvk",
											"CUP_srifle_VSSVintorez",
											"CUP_srifle_AS50",
											"CUP_arifle_AK74",
											"CUP_arifle_AK107",
											"CUP_arifle_AK107_GL",
											"CUP_arifle_AKS74",
											"CUP_arifle_AKS74U",
											"CUP_arifle_AK74_GL",
											"CUP_arifle_AKM",
											"CUP_arifle_AKS",
											"CUP_arifle_AKS_Gold",
											"CUP_arifle_RPK74",
											"CUP_arifle_CZ805_A2",
											"CUP_arifle_FNFAL",
											"CUP_arifle_FNFAL_railed",
											"CUP_arifle_G36A",
											"CUP_arifle_G36A_camo",
											"CUP_arifle_G36K",
											"CUP_arifle_G36K_camo",
											"CUP_arifle_G36C",
											"CUP_arifle_G36C_camo",
											"CUP_arifle_MG36",
											"CUP_arifle_MG36_camo",
											"CUP_arifle_L85A2",
											"CUP_arifle_L85A2_GL",
											"CUP_arifle_L86A2",
											"CUP_arifle_M16A2",
											"CUP_arifle_M16A2_GL",
											"CUP_arifle_M16A4_GL",
											"CUP_arifle_M4A1",
											"CUP_arifle_M4A1_camo",
											"CUP_arifle_M16A4_Base",
											"CUP_arifle_M4A1_BUIS_GL",
											"CUP_arifle_M4A1_BUIS_camo_GL",
											"CUP_arifle_M4A1_BUIS_desert_GL",
											"CUP_arifle_M4A1_black",
											"CUP_arifle_M4A1_desert",
											"CUP_arifle_Sa58P",
											"CUP_arifle_Sa58V",
											"CUP_arifle_Mk16_CQC",
											"CUP_arifle_XM8_Compact_Rail",
											"CUP_arifle_XM8_Railed",
											"CUP_arifle_XM8_Carbine",
											"CUP_arifle_XM8_Carbine_FG",
											"CUP_arifle_XM8_Carbine_GL",
											"CUP_arifle_XM8_Compact",
											"CUP_arifle_xm8_SAW",
											"CUP_arifle_xm8_sharpshooter",
											"CUP_arifle_CZ805_A1",
											"CUP_arifle_CZ805_GL",
											"CUP_arifle_CZ805_B_GL",
											"CUP_arifle_CZ805_B",
											"CUP_arifle_Sa58P_des",
											"CUP_arifle_Sa58V_camo",
											"CUP_arifle_Sa58RIS1",
											"CUP_arifle_Sa58RIS2",
											"CUP_arifle_Sa58RIS2_camo",
											"CUP_arifle_Mk16_CQC_FG",
											"CUP_arifle_Mk16_CQC_SFG",
											"CUP_arifle_Mk16_CQC_EGLM",
											"CUP_arifle_Mk16_STD",
											"CUP_arifle_Mk16_STD_FG",
											"CUP_arifle_Mk16_STD_SFG",
											"CUP_arifle_Mk16_STD_EGLM",
											"CUP_arifle_Mk16_SV",
											"CUP_arifle_Mk17_CQC",
											"CUP_arifle_Mk17_CQC_FG",
											"CUP_arifle_Mk17_CQC_SFG",
											"CUP_arifle_Mk17_CQC_EGLM",
											"CUP_arifle_Mk17_STD",
											"CUP_arifle_Mk17_STD_FG",
											"CUP_arifle_Mk17_STD_SFG",
											"CUP_arifle_Mk17_STD_EGLM",
											"CUP_arifle_Mk20"
										];
	DMS_assault_pistols =				[							// Pistols for Assault Class (Set to empty array if you don't want to give them any pistols)
											"hgun_ACPC2_F",
											"hgun_Rook40_F",
											"hgun_P07_F",
											"hgun_Pistol_heavy_01_F",
											"hgun_Pistol_heavy_02_F",
											"Exile_Weapon_Colt1911",
											"Exile_Weapon_Makarov",
											"Exile_Weapon_Taurus",
											"Exile_Weapon_TaurusGold",
											"CUP_hgun_Colt1911",
											"CUP_hgun_Compact",
											"CUP_hgun_Glock17",
											"CUP_hgun_M9",
											"CUP_hgun_Makarov",
											"CUP_hgun_PB6P9",
											"CUP_hgun_MicroUzi",
											"CUP_hgun_TaurusTracker455",
											"CUP_hgun_TaurusTracker455_gold",
											"CUP_hgun_SA61",
											"CUP_hgun_Duty",
											"CUP_hgun_Phantom",
											"rhs_weap_pya",
											"rhs_weap_makarov_pm",
											"rhs_weap_pp2000_folded",
											"rhsusf_weap_glock17g4",
											"rhsusf_weap_m9",
											"rhsusf_weap_m1911a1",
											"rhsusf_weap_glock17g4",
											"rhsusf_weap_m9",
											"rhsusf_weap_m1911a1"
										];
	DMS_assault_optics =				[							// Optics for Assault Class
											#ifdef GIVE_AI_APEX_GEAR
											"optic_ERCO_khk_F",
											"optic_Holosight_blk_F",
											#endif
											"optic_Arco",
											"optic_Hamr",
											"optic_Aco",
											"optic_Holosight",
											"optic_MRCO",
											"optic_DMS",
											"CUP_optic_PSO_1",
											"CUP_optic_PSO_3",
											"CUP_optic_Kobra",
											"CUP_optic_GOSHAWK",
											"CUP_optic_NSPU",
											"CUP_optic_PechenegScope",
											"CUP_optic_MAAWS_Scope",
											"CUP_optic_SMAW_Scope",
											"CUP_optic_AN_PAS_13c2",
											"CUP_optic_LeupoldMk4",
											"CUP_optic_HoloBlack",
											"CUP_optic_HoloWdl",
											"CUP_optic_HoloDesert",
											"CUP_optic_Eotech533",
											"CUP_optic_CompM4",
											"CUP_optic_SUSAT",
											"CUP_optic_ACOG",
											"CUP_optic_CWS",
											"CUP_optic_Leupold_VX3",
											"CUP_optic_AN_PVS_10",
											"CUP_optic_CompM2_Black",
											"CUP_optic_CompM2_Woodland",
											"CUP_optic_CompM2_Woodland2",
											"CUP_optic_CompM2_Desert",
											"CUP_optic_RCO",
											"CUP_optic_RCO_desert",
											"CUP_optic_LeupoldM3LR",
											"CUP_optic_LeupoldMk4_10x40_LRT_Desert",
											"CUP_optic_LeupoldMk4_10x40_LRT_Woodland",
											"CUP_optic_ElcanM145",
											"CUP_optic_AN_PAS_13c1",
											"CUP_optic_LeupoldMk4_CQ_T",
											"CUP_optic_ELCAN_SpecterDR",
											"CUP_optic_LeupoldMk4_MRT_tan",
											"CUP_optic_SB_11_4x20_PM",
											"CUP_optic_ZDDot",
											"CUP_optic_MRad",
											"CUP_optic_TrijiconRx01_desert",
											"CUP_optic_TrijiconRx01_black",
											"CUP_optic_AN_PVS_4"
										];
	DMS_assault_optic_chance			= 75;						// Percentage chance that an Assault Class AI will get an optic
	DMS_assault_bipod_chance			= 25;						// Percentage chance that an Assault Class AI will get a bipod
	DMS_assault_suppressor_chance		= 25;						// Percentage chance that an Assault Class AI will get a suppressor

	DMS_assault_items =					[							// Items for Assault Class AI (Loot stuff that goes in uniform/vest/backpack)
											"Exile_Item_InstaDoc",
											"Exile_Item_BBQSandwich",
											"Exile_Item_Energydrink"
										];
	DMS_assault_equipment =				[							// Equipment for Assault Class AI (stuff that goes in toolbelt slots)
											"ItemGPS"
										];
	DMS_assault_RandItemCount =			2;							// How many random items to add to the AI's inventory.
	DMS_assault_RandItems =				[							// The random items that will be added to the AI's inventory.
											"Exile_Item_Catfood_Cooked",
											"Exile_Item_Surstromming_Cooked",
											"Exile_Item_PowerDrink",
											"Exile_Item_EnergyDrink",
											"Exile_Item_Vishpirin",
											"Exile_Item_Bandage"
										];
	DMS_assault_helmets	=				[							// Helmets for Assault Class
											#ifdef GIVE_AI_APEX_GEAR
											"H_HelmetB_TI_tna_F",
											"H_HelmetB_Enh_tna_F",
											"H_HelmetSpecO_ghex_F",
											"H_HelmetCrew_O_ghex_F",
											#endif
											"H_HelmetSpecB_paint1",
											"H_HelmetIA_camo",
											"H_HelmetLeaderO_ocamo",
											"H_HelmetLeaderO_oucamo",
											"HAP_HPILOT_big_smile1",
											"HAP_HPILOT_green",
											"HAP_HPILOT_hex_camo1",
											"HAP_HPILOT_hex_camo2",
											"HAP_HPILOT_hex_camo3",
											"HAP_HPILOT_orange",
											"HAP_HPILOT_predator",
											"HAP_HPILOT_robot1",
											"HAP_HPILOT_rough_teeth1",
											"HAP_HPILOT_smile",
											"HAP_HPILOT_smile2",
											"HAP_HPILOT_teal",
											"HAP_MOTO_BLACK",
											"HAP_MOTO_BLACKNRED",
											"HAP_MOTO_DASH",
											"HAP_MOTO_FLAME1",
											"HAP_MOTO_GREEN",
											"HAP_MOTO_PAT",
											"HAP_MOTO_PINK",
											"HAP_MOTO_PLUM",
											"HAP_MOTO_STUNT",
											"CUP_H_BAF_Helmet_1_DDPM",
											"CUP_H_BAF_Helmet_1_DPM",
											"CUP_H_BAF_Helmet_1_MTP",
											"CUP_H_BAF_Helmet_2_DDPM",
											"CUP_H_BAF_Helmet_2_DPM",
											"CUP_H_BAF_Helmet_2_MTP",
											"CUP_H_BAF_Helmet_3_DDPM",
											"CUP_H_BAF_Helmet_3_DPM",
											"CUP_H_BAF_Helmet_3_MTP",
											"CUP_H_BAF_Helmet_4_DDPM",
											"CUP_H_BAF_Helmet_4_DPM",
											"CUP_H_BAF_Helmet_4_MTP",
											"CUP_H_BAF_Officer_Beret_PRR_O",
											"CUP_H_C_Beanie_01",
											"CUP_H_C_Beanie_02",
											"CUP_H_C_Beanie_03",
											"CUP_H_C_Beanie_04",
											"CUP_H_C_Beret_01",
											"CUP_H_C_Beret_02",
											"CUP_H_C_Beret_03",
											"CUP_H_C_Beret_04",
											"CUP_H_C_Ushanka_01",
											"CUP_H_C_Ushanka_02",
											"CUP_H_C_Ushanka_03",
											"CUP_H_C_Ushanka_04",
											"CUP_H_FR_BandanaGreen",
											"CUP_H_FR_BandanaWdl",
											"CUP_H_FR_Bandana_Headset",
											"CUP_H_FR_BeanieGreen",
											"CUP_H_FR_BoonieMARPAT",
											"CUP_H_FR_BoonieWDL",
											"CUP_H_FR_Cap_Headset_Green",
											"CUP_H_FR_Cap_Officer_Headset",
											"CUP_H_FR_ECH",
											"CUP_H_FR_Headband_Headset",
											"CUP_H_FR_Headset",
											"CUP_H_FR_PRR_BoonieWDL",
											"CUP_H_GER_Boonie_Flecktarn",
											"CUP_H_GER_Boonie_desert",
											"CUP_H_NAPA_Fedora",
											"CUP_H_Navy_CrewHelmet_Blue",
											"CUP_H_Navy_CrewHelmet_Brown",
											"CUP_H_Navy_CrewHelmet_Green",
											"CUP_H_Navy_CrewHelmet_Red",
											"CUP_H_Navy_CrewHelmet_Violet",
											"CUP_H_Navy_CrewHelmet_White",
											"CUP_H_Navy_CrewHelmet_Yellow",
											"CUP_H_PMC_Cap_Grey",
											"CUP_H_PMC_Cap_PRR_Grey",
											"CUP_H_PMC_Cap_PRR_Tan",
											"CUP_H_PMC_Cap_Tan",
											"CUP_H_PMC_EP_Headset",
											"CUP_H_PMC_PRR_Headset",
											"CUP_H_RACS_Beret_Blue",
											"CUP_H_RACS_Helmet_DPAT",
											"CUP_H_RACS_Helmet_Des",
											"CUP_H_RACS_Helmet_Goggles_DPAT",
											"CUP_H_RACS_Helmet_Goggles_Des",
											"CUP_H_RACS_Helmet_Headset_DPAT",
											"CUP_H_RACS_Helmet_Headset_Des",
											"CUP_H_SLA_BeenieGreen",
											"CUP_H_SLA_Beret",
											"CUP_H_SLA_Boonie",
											"CUP_H_SLA_Helmet",
											"CUP_H_SLA_OfficerCap",
											"CUP_H_SLA_Pilot_Helmet",
											"CUP_H_SLA_SLCap",
											"CUP_H_SLA_TankerHelmet",
											"CUP_H_TKI_Lungee_01",
											"CUP_H_TKI_Lungee_02",
											"CUP_H_TKI_Lungee_03",
											"CUP_H_TKI_Lungee_04",
											"CUP_H_TKI_Lungee_05",
											"CUP_H_TKI_Lungee_06",
											"CUP_H_TKI_Lungee_Open_01",
											"CUP_H_TKI_Lungee_Open_02",
											"CUP_H_TKI_Lungee_Open_03",
											"CUP_H_TKI_Lungee_Open_04",
											"CUP_H_TKI_Lungee_Open_05",
											"CUP_H_TKI_Lungee_Open_06",
											"CUP_H_TKI_Pakol_1_01",
											"CUP_H_TKI_Pakol_1_02",
											"CUP_H_TKI_Pakol_1_03",
											"CUP_H_TKI_Pakol_1_04",
											"CUP_H_TKI_Pakol_1_05",
											"CUP_H_TKI_Pakol_1_06",
											"CUP_H_TKI_Pakol_2_01",
											"CUP_H_TKI_Pakol_2_02",
											"CUP_H_TKI_Pakol_2_03",
											"CUP_H_TKI_Pakol_2_04",
											"CUP_H_TKI_Pakol_2_05",
											"CUP_H_TKI_Pakol_2_06",
											"CUP_H_TKI_SkullCap_01",
											"CUP_H_TKI_SkullCap_02",
											"CUP_H_TKI_SkullCap_03",
											"CUP_H_TKI_SkullCap_04",
											"CUP_H_TKI_SkullCap_05",
											"CUP_H_TKI_SkullCap_06",
											"CUP_H_TK_Beret",
											"CUP_H_TK_Helmet",
											"CUP_H_TK_Lungee",
											"CUP_H_TK_PilotHelmet",
											"CUP_H_TK_TankerHelmet",
											"CUP_H_USMC_Crew_Helmet",
											"CUP_H_USMC_Goggles_HelmetWDL",
											"CUP_H_USMC_HeadSet_GoggleW_HelmetWDL",
											"CUP_H_USMC_HeadSet_HelmetWDL",
											"CUP_H_USMC_HelmetWDL",
											"CUP_H_USMC_Helmet_Pilot",
											"CUP_H_USMC_Officer_Cap",
											"rhs_6b27m_digi",
											"rhs_6b27m_digi_ess",
											"rhs_6b27m_digi_bala",
											"rhs_6b27m_digi_ess_bala",
											"rhs_6b27m",
											"rhs_6b27m_bala",
											"rhs_6b27m_ess",
											"rhs_6b27m_ess_bala",
											"rhs_6b27m_ml",
											"rhs_6b27m_ml_ess",
											"rhs_6b27m_ml_bala",
											"rhs_6b27m_ML_ess_bala",
											"rhs_6b27m_green",
											"rhs_6b27m_green_ess",
											"rhs_6b27m_green_ess_bala",
											"rhs_6b26_green",
											"rhs_6b26_bala_green",
											"rhs_6b26_ess_green",
											"rhs_6b26_ess_bala_green",
											"rhs_6b26",
											"rhs_6b26_bala",
											"rhs_6b26_ess",
											"rhs_6b26_ess_bala",
											"rhs_6b28_green",
											"rhs_6b28_green_bala",
											"rhs_6b28_green_ess",
											"rhs_6b28_green_ess_bala",
											"rhs_6b28",
											"rhs_6b28_bala",
											"rhs_6b28_ess",
											"rhs_6b28_ess_bala",
											"rhs_6b28_flora",
											"rhs_6b28_flora_bala",
											"rhs_6b28_flora_ess",
											"rhs_6b28_flora_ess_bala",
											"rhs_Booniehat_digi",
											"rhs_Booniehat_flora",
											"rhs_ssh68",
											"rhs_Booniehat_m81",
											"rhs_Booniehat_marpatd",
											"rhs_Booniehat_marpatwd",
											"rhs_Booniehat_ocp",
											"rhs_Booniehat_ucp",
											"rhsusf_Bowman",
											"rhsusf_ach_bare",
											"rhsusf_ach_bare_des",
											"rhsusf_ach_bare_des_ess",
											"rhsusf_ach_bare_des_headset",
											"rhsusf_ach_bare_des_headset_ess",
											"rhsusf_ach_bare_ess",
											"rhsusf_ach_bare_headset",
											"rhsusf_ach_bare_headset_ess",
											"rhsusf_ach_bare_semi",
											"rhsusf_ach_bare_semi_ess",
											"rhsusf_ach_bare_semi_headset",
											"rhsusf_ach_bare_semi_headset_ess",
											"rhsusf_ach_bare_tan",
											"rhsusf_ach_bare_tan_ess",
											"rhsusf_ach_bare_tan_headset",
											"rhsusf_ach_bare_tan_headset_ess",
											"rhsusf_ach_bare_wood",
											"rhsusf_ach_bare_wood_ess",
											"rhsusf_ach_bare_wood_headset",
											"rhsusf_ach_bare_wood_headset_ess",
											"rhsusf_ach_helmet_ESS_ocp",
											"rhsusf_ach_helmet_ESS_ucp",
											"rhsusf_ach_helmet_M81",
											"rhsusf_ach_helmet_camo_ocp",
											"rhsusf_ach_helmet_headset_ess_ocp",
											"rhsusf_ach_helmet_headset_ess_ucp",
											"rhsusf_ach_helmet_headset_ocp",
											"rhsusf_ach_helmet_headset_ucp",
											"rhsusf_ach_helmet_ocp",
											"rhsusf_ach_helmet_ocp_norotos",
											"rhsusf_ach_helmet_ucp",
											"rhsusf_ach_helmet_ucp_norotos",
											"rhsusf_bowman_cap",
											"rhsusf_lwh_helmet_M1942",
											"rhsusf_lwh_helmet_marpatd",
											"rhsusf_lwh_helmet_marpatd_ess",
											"rhsusf_lwh_helmet_marpatd_headset",
											"rhsusf_lwh_helmet_marpatwd",
											"rhsusf_lwh_helmet_marpatwd_ess",
											"rhsusf_lwh_helmet_marpatwd_headset",
											"rhsusf_mich_bare",
											"rhsusf_mich_bare_alt",
											"rhsusf_mich_bare_alt_semi",
											"rhsusf_mich_bare_alt_tan",
											"rhsusf_mich_bare_headset",
											"rhsusf_mich_bare_norotos",
											"rhsusf_mich_bare_norotos_alt",
											"rhsusf_mich_bare_norotos_alt_headset",
											"rhsusf_mich_bare_norotos_alt_semi",
											"rhsusf_mich_bare_norotos_alt_semi_headset",
											"rhsusf_mich_bare_norotos_alt_tan",
											"rhsusf_mich_bare_norotos_alt_tan_headset",
											"rhsusf_mich_bare_norotos_arc",
											"rhsusf_mich_bare_norotos_arc_alt",
											"rhsusf_mich_bare_norotos_arc_alt_headset",
											"rhsusf_mich_bare_norotos_arc_alt_semi",
											"rhsusf_mich_bare_norotos_arc_alt_semi_headset",
											"rhsusf_mich_bare_norotos_arc_alt_tan",
											"rhsusf_mich_bare_norotos_arc_alt_tan_headset",
											"rhsusf_mich_bare_norotos_arc_headset",
											"rhsusf_mich_bare_norotos_arc_semi",
											"rhsusf_mich_bare_norotos_arc_semi_headset",
											"rhsusf_mich_bare_norotos_arc_tan",
											"rhsusf_mich_bare_norotos_headset",
											"rhsusf_mich_bare_norotos_semi",
											"rhsusf_mich_bare_norotos_semi_headset",
											"rhsusf_mich_bare_norotos_tan",
											"rhsusf_mich_bare_norotos_tan_headset",
											"rhsusf_mich_bare_semi",
											"rhsusf_mich_bare_semi_headset",
											"rhsusf_mich_bare_tan",
											"rhsusf_mich_bare_tan_headset",
											"rhsusf_mich_helmet_marpatd_alt_headset",
											"rhsusf_mich_helmet_marpatd_headset",
											"rhsusf_mich_helmet_marpatd_norotos",
											"rhsusf_mich_helmet_marpatd_norotos_arc",
											"rhsusf_mich_helmet_marpatd_norotos_arc_headset",
											"rhsusf_mich_helmet_marpatd_norotos_headset",
											"rhsusf_mich_helmet_marpatwd",
											"rhsusf_mich_helmet_marpatwd_alt",
											"rhsusf_mich_helmet_marpatwd_alt_headset",
											"rhsusf_mich_helmet_marpatwd_headset",
											"rhsusf_mich_helmet_marpatwd_norotos",
											"rhsusf_mich_helmet_marpatwd_norotos_arc",
											"rhsusf_mich_helmet_marpatwd_norotos_arc_headset",
											"rhsusf_mich_helmet_marpatwd_norotos_headset",
											"rhs_altyn_novisor",
											"rhs_altyn_novisor_bala",
											"rhs_altyn_novisor_ess",
											"rhs_altyn_novisor_ess_bala",
											"rhs_altyn_visordown",
											"rhs_altyn",
											"rhs_altyn_bala",
											"rhsusf_opscore_bk_pelt",
											"rhsusf_opscore_bk",
											"rhsusf_opscore_coy_cover",
											"rhsusf_opscore_coy_cover_pelt",
											"rhsusf_opscore_fg",
											"rhsusf_opscore_fg_pelt",
											"rhsusf_opscore_fg_pelt_cam",
											"rhsusf_opscore_fg_pelt_nsw",
											"rhsusf_opscore_mc",
											"rhsusf_opscore_mc_pelt",
											"rhsusf_opscore_mc_pelt_nsw",
											"rhsusf_opscore_mc_cover",
											"rhsusf_opscore_mc_cover_pelt",
											"rhsusf_opscore_mc_cover_pelt_nsw",
											"rhsusf_opscore_mc_cover_pelt_cam",
											"rhsusf_opscore_paint",
											"rhsusf_opscore_paint_pelt",
											"rhsusf_opscore_paint_pelt_nsw",
											"rhsusf_opscore_paint_pelt_nsw_cam",
											"rhsusf_opscore_rg_cover",
											"rhsusf_opscore_rg_cover_pelt",
											"rhsusf_opscore_ut",
											"rhsusf_opscore_ut_pelt",
											"rhsusf_opscore_ut_pelt_cam",
											"rhsusf_opscore_ut_pelt_nsw",
											"rhsusf_opscore_ut_pelt_nsw_cam",
											"rhsusf_opscore_mar_ut_pelt",
											"rhsusf_opscore_mar_ut",
											"rhsusf_opscore_mar_fg_pelt",
											"rhsusf_opscore_mar_fg",
											"rhsusf_protech_helmet",
											"rhsusf_protech_helmet_ess",
											"rhsusf_protech_helmet_rhino",
											"rhsusf_protech_helmet_rhino_ess",
											"rhsgref_6b27m",
											"rhsgref_6b27m_ttsko_digi",
											"rhsgref_6b27m_ttsko_forest",
											"rhsgref_6b27m_ttsko_mountain",
											"rhsgref_6b27m_ttsko_urban",
											"rhsgref_Booniehat_alpen",
											"rhsgref_fieldcap",
											"rhsgref_fieldcap_ttsko_digi",
											"rhsgref_fieldcap_ttsko_forest",
											"rhsgref_fieldcap_ttsko_mountain",
											"rhsgref_fieldcap_ttsko_urban",
											"rhsgref_patrolcap_specter",
											"rhsgref_ssh68",
											"rhsgref_ssh68_emr",
											"rhsgref_ssh68_ttsko_digi",
											"rhsgref_ssh68_ttsko_forest",
											"rhsgref_ssh68_ttsko_mountain",
											"rhsgref_ssh68_un",
											"rhssaf_helmet_m59_85_nocamo",
											"rhssaf_helmet_m59_85_oakleaf",
											"rhssaf_helmet_m97_olive_nocamo",
											"rhssaf_helmet_m97_olive_nocamo_black_ess",
											"rhssaf_helmet_m97_olive_nocamo_black_ess_bare",
											"rhssaf_helmet_m97_black_nocamo",
											"rhssaf_helmet_m97_black_nocamo_black_ess",
											"rhssaf_helmet_m97_black_nocamo_black_ess_bare",
											"rhssaf_Helmet_m97_woodland",
											"rhssaf_Helmet_m97_digital",
											"rhssaf_Helmet_m97_md2camo",
											"rhssaf_Helmet_m97_oakleaf",
											"rhssaf_helmet_m97_nostrap_blue",
											"rhssaf_helmet_m97_nostrap_blue_tan_ess",
											"rhssaf_helmet_m97_nostrap_blue_tan_ess_bare",
											"rhssaf_helmet_m97_woodland_black_ess",
											"rhssaf_helmet_m97_woodland_black_ess_bare",
											"rhssaf_helmet_m97_digital_black_ess",
											"rhssaf_helmet_m97_digital_black_ess_bare",
											"rhssaf_helmet_m97_md2camo_black_ess",
											"rhssaf_helmet_m97_md2camo_black_ess_bare",
											"rhssaf_helmet_m97_oakleaf_black_ess",
											"rhssaf_helmet_m97_oakleaf_black_ess_bare",
											"rhssaf_helmet_hgu56p",
											"rhssaf_beret_green",
											"rhssaf_beret_red",
											"rhssaf_beret_black",
											"rhssaf_beret_blue_un",
											"rhssaf_booniehat_digital",
											"rhssaf_booniehat_md2camo",
											"rhssaf_booniehat_woodland",
											"rhs_Booniehat_m81",
											"rhs_Booniehat_marpatd",
											"rhs_Booniehat_marpatwd",
											"rhs_Booniehat_ocp",
											"rhs_Booniehat_ucp",
											"rhsusf_Bowman",
											"rhsusf_ach_bare",
											"rhsusf_ach_bare_des",
											"rhsusf_ach_bare_des_ess",
											"rhsusf_ach_bare_des_headset",
											"rhsusf_ach_bare_des_headset_ess",
											"rhsusf_ach_bare_ess",
											"rhsusf_ach_bare_headset",
											"rhsusf_ach_bare_headset_ess",
											"rhsusf_ach_bare_semi",
											"rhsusf_ach_bare_semi_ess",
											"rhsusf_ach_bare_semi_headset",
											"rhsusf_ach_bare_semi_headset_ess",
											"rhsusf_ach_bare_tan",
											"rhsusf_ach_bare_tan_ess",
											"rhsusf_ach_bare_tan_headset",
											"rhsusf_ach_bare_tan_headset_ess",
											"rhsusf_ach_bare_wood",
											"rhsusf_ach_bare_wood_ess",
											"rhsusf_ach_bare_wood_headset",
											"rhsusf_ach_bare_wood_headset_ess",
											"rhsusf_ach_helmet_ESS_ocp",
											"rhsusf_ach_helmet_ESS_ucp",
											"rhsusf_ach_helmet_M81",
											"rhsusf_ach_helmet_camo_ocp",
											"rhsusf_ach_helmet_headset_ess_ocp",
											"rhsusf_ach_helmet_headset_ess_ucp",
											"rhsusf_ach_helmet_headset_ocp",
											"rhsusf_ach_helmet_headset_ucp",
											"rhsusf_ach_helmet_ocp",
											"rhsusf_ach_helmet_ocp_norotos",
											"rhsusf_ach_helmet_ucp",
											"rhsusf_ach_helmet_ucp_norotos",
											"rhsusf_bowman_cap",
											"rhsusf_lwh_helmet_M1942",
											"rhsusf_lwh_helmet_marpatd",
											"rhsusf_lwh_helmet_marpatd_ess",
											"rhsusf_lwh_helmet_marpatd_headset",
											"rhsusf_lwh_helmet_marpatwd",
											"rhsusf_lwh_helmet_marpatwd_ess",
											"rhsusf_lwh_helmet_marpatwd_headset",
											"rhsusf_mich_bare",
											"rhsusf_mich_bare_alt",
											"rhsusf_mich_bare_alt_semi",
											"rhsusf_mich_bare_alt_tan",
											"rhsusf_mich_bare_headset",
											"rhsusf_mich_bare_norotos",
											"rhsusf_mich_bare_norotos_alt",
											"rhsusf_mich_bare_norotos_alt_headset",
											"rhsusf_mich_bare_norotos_alt_semi",
											"rhsusf_mich_bare_norotos_alt_semi_headset",
											"rhsusf_mich_bare_norotos_alt_tan",
											"rhsusf_mich_bare_norotos_alt_tan_headset",
											"rhsusf_mich_bare_norotos_arc",
											"rhsusf_mich_bare_norotos_arc_alt",
											"rhsusf_mich_bare_norotos_arc_alt_headset",
											"rhsusf_mich_bare_norotos_arc_alt_semi",
											"rhsusf_mich_bare_norotos_arc_alt_semi_headset",
											"rhsusf_mich_bare_norotos_arc_alt_tan",
											"rhsusf_mich_bare_norotos_arc_alt_tan_headset",
											"rhsusf_mich_bare_norotos_arc_headset",
											"rhsusf_mich_bare_norotos_arc_semi",
											"rhsusf_mich_bare_norotos_arc_semi_headset",
											"rhsusf_mich_bare_norotos_arc_tan",
											"rhsusf_mich_bare_norotos_headset",
											"rhsusf_mich_bare_norotos_semi",
											"rhsusf_mich_bare_norotos_semi_headset",
											"rhsusf_mich_bare_norotos_tan",
											"rhsusf_mich_bare_norotos_tan_headset",
											"rhsusf_mich_bare_semi",
											"rhsusf_mich_bare_semi_headset",
											"rhsusf_mich_bare_tan",
											"rhsusf_mich_bare_tan_headset",
											"rhsusf_mich_helmet_marpatd_alt_headset",
											"rhsusf_mich_helmet_marpatd_headset",
											"rhsusf_mich_helmet_marpatd_norotos",
											"rhsusf_mich_helmet_marpatd_norotos_arc",
											"rhsusf_mich_helmet_marpatd_norotos_arc_headset",
											"rhsusf_mich_helmet_marpatd_norotos_headset",
											"rhsusf_mich_helmet_marpatwd",
											"rhsusf_mich_helmet_marpatwd_alt",
											"rhsusf_mich_helmet_marpatwd_alt_headset",
											"rhsusf_mich_helmet_marpatwd_headset",
											"rhsusf_mich_helmet_marpatwd_norotos",
											"rhsusf_mich_helmet_marpatwd_norotos_arc",
											"rhsusf_mich_helmet_marpatwd_norotos_arc_headset",
											"rhsusf_mich_helmet_marpatwd_norotos_headset",
											"rhsusf_patrolcap_ocp",
											"rhsusf_patrolcap_ucp",
											"rhsusf_opscore_01",
											"rhsusf_opscore_01_tan",
											"rhsusf_opscore_02_tan",
											"rhsusf_opscore_03_ocp",
											"rhsusf_opscore_04_ocp",
											"rhsusf_cvc_helmet",
											"rhsusf_cvc_ess",
											"rhsusf_hgu56p",
											"rhsusf_hgu56p_mask",
											"rhsusf_cvc_green_helmet",
											"rhsusf_cvc_green_ess",
											"rhsusf_opscore_bk_pelt",
											"rhsusf_opscore_bk",
											"rhsusf_opscore_coy_cover",
											"rhsusf_opscore_coy_cover_pelt",
											"rhsusf_opscore_fg",
											"rhsusf_opscore_fg_pelt",
											"rhsusf_opscore_fg_pelt_cam",
											"rhsusf_opscore_fg_pelt_nsw",
											"rhsusf_opscore_mc",
											"rhsusf_opscore_mc_pelt",
											"rhsusf_opscore_mc_pelt_nsw",
											"rhsusf_opscore_mc_cover",
											"rhsusf_opscore_mc_cover_pelt",
											"rhsusf_opscore_mc_cover_pelt_nsw",
											"rhsusf_opscore_mc_cover_pelt_cam",
											"rhsusf_opscore_paint",
											"rhsusf_opscore_paint_pelt",
											"rhsusf_opscore_paint_pelt_nsw",
											"rhsusf_opscore_paint_pelt_nsw_cam",
											"rhsusf_opscore_rg_cover",
											"rhsusf_opscore_rg_cover_pelt",
											"rhsusf_opscore_ut",
											"rhsusf_opscore_ut_pelt",
											"rhsusf_opscore_ut_pelt_cam",
											"rhsusf_opscore_ut_pelt_nsw",
											"rhsusf_opscore_ut_pelt_nsw_cam",
											"rhsusf_opscore_mar_ut_pelt",
											"rhsusf_opscore_mar_ut",
											"rhsusf_opscore_mar_fg_pelt",
											"rhsusf_opscore_mar_fg",
											"rhsusf_protech_helmet",
											"rhsusf_protech_helmet_ess",
											"rhsusf_protech_helmet_rhino",
											"rhsusf_protech_helmet_rhino_ess"
										];
	DMS_assault_clothes	=				[							// Uniforms for Assault Class
											#ifdef GIVE_AI_APEX_GEAR
											"U_B_T_Soldier_F",
											"U_B_T_Soldier_SL_F",
											"U_B_CTRG_Soldier_F",
											"U_O_V_Soldier_Viper_F",
											"U_I_C_Soldier_Bandit_2_F",
											"U_I_C_Soldier_Camo_F",
											"U_B_CTRG_Soldier_urb_1_F",
											#endif
											"U_O_CombatUniform_ocamo",
											"U_O_PilotCoveralls",
											//"U_B_Wetsuit",
											"U_BG_Guerilla3_1",
											"U_BG_Guerilla2_3",
											"U_BG_Guerilla2_2",
											"U_BG_Guerilla1_1",
											"U_BG_Guerrilla_6_1",
											"U_IG_Guerilla3_2",
											"U_B_SpecopsUniform_sgg",
											"U_I_OfficerUniform",
											"U_B_CTRG_3",
											"U_I_G_resistanceLeader_F",
											"U_IG_Guerilla1_1",
											"U_IG_Guerilla2_1",
											"U_IG_Guerilla2_2",
											"U_IG_Guerilla2_3",
											"U_IG_Guerilla3_1",
											"U_BG_Guerilla2_1",
											"U_IG_Guerilla3_2",
											"U_BG_Guerrilla_6_1",
											"U_BG_Guerilla1_1",
											"U_BG_Guerilla2_2",
											"U_BG_Guerilla2_3",
											"U_BG_Guerilla3_1",
											"HAP_BTN_army",
											"HAP_BTN_haw1",
											"HAP_BTN_haw2",
											"HAP_BTN_hp1",
											"HAP_BTN_pld_blu1",
											"HAP_BTN_pld_blu2",
											"HAP_BTN_pld_orng1",
											"HAP_BTN_pld_red1",
											"HAP_BTN_prp1",
											"HAP_CIV2_basic",
											"HAP_CIV2_ob_striped",
											"HAP_CIV3_basic",
											"HAP_CIV3_blue",
											"HAP_CIV3_camo1",
											"HAP_CIV3_green",
											"HAP_CIV3_redPlaid",
											"HAP_CIV3_snow1",
											"HAP_CIV4_basic",
											"HAP_CIV5_basic",
											"HAP_CIV5_brown",
											"HAP_CIV5_snow1",
											"HAP_CIV5_soft_blue",
											"HAP_FAT_black1",
											"HAP_FAT_blu1",
											"HAP_FAT_dea",
											"HAP_FAT_orn1",
											"HAP_FAT_ref",
											"HAP_FAT_snow",
											"HAP_GUE_pun",
											"HAP_OF1_WOOD_CombatUniform",
											"HAP_OF1_snow_CombatUniform",
											"HAP_PARA_basic",
											"HAP_PARA_blue",
											"HAP_PARA_camo1",
											"HAP_PARA_camo2",
											"HAP_PARA_camo3",
											"HAP_PARA_camo4",
											"HAP_POLO_blackNwinter",
											"HAP_POLO_blackNwood",
											"HAP_POLO_bowler1",
											"HAP_POLO_bowler2",
											"HAP_POLO_bowler3",
											"HAP_POLO_cbrown",
											"HAP_POLO_hello",
											"HAP_POLO_sox",
											"HAP_POLO_yank",
											"HAP_SNIPER_black",
											"HAP_SNIPER_top_skull1",
											"HAP_TEE2_Gcamo1",
											"HAP_TEE2_Gcamo2",
											"HAP_TEE2_HUNTER_1_1",
											"HAP_TEE2_HUNTER_1_2",
											"HAP_TEE2_HUNTER_1_3",
											"HAP_TEE2_HUNTER_1_4",
											"HAP_TEE2_HUNTER_1_5",
											"HAP_TEE2_HUNTER_1_6",
											"HAP_TEE2_black",
											"HAP_TEE2_bloody",
											"HAP_TEE2_greygrn",
											"HAP_TEE2_prpl",
											"HAP_TEE2_tiedye",
											"HAP_TEE_HUNTER_2_1",
											"HAP_TEE_HUNTER_2_2",
											"HAP_TEE_HUNTER_2_3",
											"HAP_TEE_HUNTER_2_4",
											"HAP_TEE_HUNTER_2_5",
											"HAP_TEE_HUNTER_2_6",
											"HAP_TEE_boston",
											"HAP_TEE_hobo",
											"HAP_TEE_maxx",
											"HAP_TEE_mooby",
											"HAP_TEE_orngNblu",
											"HAP_TEE_tiedye",
											"HAP_TEE_tricamo1",
											"HAP_TEE_vault",
											"HAP_TEE_yellowNgrn",
											"CUP_O_TKI_Khet_Partug_01",
											"CUP_O_TKI_Khet_Partug_02",
											"CUP_O_TKI_Khet_Partug_03",
											"CUP_O_TKI_Khet_Partug_04",
											"CUP_O_TKI_Khet_Partug_05",
											"CUP_O_TKI_Khet_Partug_06",
											"CUP_O_TKI_Khet_Partug_07",
											"CUP_O_TKI_Khet_Partug_08",
											"CUP_O_TKI_Khet_Jeans_01",
											"CUP_O_TKI_Khet_Jeans_02",
											"CUP_O_TKI_Khet_Jeans_03",
											"CUP_O_TKI_Khet_Jeans_04",
											"CUP_U_C_Pilot_01",
											"CUP_U_C_Citizen_01",
											"CUP_U_C_Citizen_02",
											"CUP_U_C_Citizen_03",
											"CUP_U_C_Citizen_04",
											"CUP_U_C_Worker_01",
											"CUP_U_C_Worker_02",
											"CUP_U_C_Worker_03",
											"CUP_U_C_Worker_04",
											"CUP_U_C_Profiteer_01",
											"CUP_U_C_Profiteer_02",
											"CUP_U_C_Profiteer_03",
											"CUP_U_C_Profiteer_04",
											"CUP_U_C_Woodlander_01",
											"CUP_U_C_Woodlander_02",
											"CUP_U_C_Woodlander_03",
											"CUP_U_C_Woodlander_04",
											"CUP_U_C_Villager_01",
											"CUP_U_C_Villager_02",
											"CUP_U_C_Villager_03",
											"CUP_U_C_Villager_04",
											"CUP_U_B_CZ_WDL_TShirt",
											"CUP_U_B_GER_Tropentarn_1",
											"CUP_U_B_GER_Tropentarn_2",
											"CUP_U_B_GER_Ghillie",
											"CUP_U_B_GER_Flecktarn_1",
											"CUP_U_B_GER_Flecktarn_2",
											"CUP_U_B_GER_Fleck_Ghillie",
											"CUP_U_B_USMC_Officer",
											"CUP_U_B_USMC_PilotOverall",
											"CUP_U_B_USMC_Ghillie_WDL",
											"CUP_U_B_USMC_MARPAT_WDL_Sleeves",
											"CUP_U_B_USMC_MARPAT_WDL_RolledUp",
											"CUP_U_B_USMC_MARPAT_WDL_Kneepad",
											"CUP_U_B_USMC_MARPAT_WDL_TwoKneepads",
											"CUP_U_B_USMC_MARPAT_WDL_RollUpKneepad",
											"CUP_U_B_FR_SpecOps",
											"CUP_U_B_FR_Scout",
											"CUP_U_B_FR_Scout1",
											"CUP_U_B_FR_Scout2",
											"CUP_U_B_FR_Scout3",
											"CUP_U_B_FR_Officer",
											"CUP_U_B_FR_Corpsman",
											"CUP_U_B_FR_DirAction",
											"CUP_U_B_FR_DirAction2",
											"CUP_U_B_FR_Light",
											"CUP_U_I_GUE_Flecktarn",
											"CUP_U_I_GUE_Flecktarn2",
											"CUP_U_I_GUE_Flecktarn3",
											"CUP_U_I_GUE_Woodland1",
											"CUP_U_I_Ghillie_Top",
											"CUP_U_I_RACS_PilotOverall",
											"CUP_U_I_RACS_Desert_1",
											"CUP_U_I_RACS_Desert_2",
											"CUP_U_I_RACS_Urban_1",
											"CUP_U_I_RACS_Urban_2",
											"CUP_U_O_SLA_Officer",
											"CUP_U_O_SLA_Officer_Suit",
											"CUP_U_O_SLA_MixedCamo",
											"CUP_U_O_SLA_Green",
											"CUP_U_O_SLA_Urban",
											"CUP_U_O_SLA_Desert",
											"CUP_U_O_SLA_Overalls_Pilot",
											"CUP_U_O_SLA_Overalls_Tank",
											"CUP_U_O_Partisan_TTsKO",
											"CUP_U_O_Partisan_TTsKO_Mixed",
											"CUP_U_O_Partisan_VSR_Mixed1",
											"CUP_U_O_Partisan_VSR_Mixed2",
											"CUP_U_O_TK_Officer",
											"CUP_U_O_TK_MixedCamo",
											"CUP_U_O_TK_Green",
											"CUP_U_O_TK_Ghillie",
											"CUP_U_O_TK_Ghillie_Top",
											"CUP_U_B_BAF_DDPM_S1_RolledUp",
											"CUP_U_B_BAF_DDPM_S1_UnRolled",
											"CUP_U_B_BAF_DDPM_Tshirt",
											"CUP_U_B_BAF_DPM_S1_RolledUp",
											"CUP_U_B_BAF_DPM_S2_UnRolled",
											"CUP_U_B_BAF_DPM_Tshirt",
											"CUP_U_B_BAF_MTP_S1_RolledUp",
											"CUP_U_B_BAF_MTP_S2_UnRolled",
											"CUP_U_B_BAF_MTP_Tshirt",
											"CUP_U_B_BAF_MTP_S3_RolledUp",
											"CUP_U_B_BAF_MTP_S4_UnRolled",
											"CUP_U_B_BAF_MTP_S5_UnRolled",
											"CUP_U_B_BAF_MTP_S6_UnRolled",
											"rhs_uniform_cu_ocp",
											"rhs_uniform_cu_ucp",
											"rhs_uniform_cu_ocp_101st",
											"rhs_uniform_df15",
											"rhs_uniform_m88_patchless",
											"rhs_uniform_emr_patchless",
											"rhs_uniform_flora_patchless",
											"rhs_uniform_flora_patchless_alt",
											"rhs_uniform_FROG01_m81",
											"rhs_uniform_FROG01_d",
											"rhs_uniform_FROG01_wd",
											"rhs_uniform_m88_patchless",
											"rhs_uniform_mflora_patchless",
											"rhs_uniform_vdv_mflora",
											"rhs_uniform_vdv_emr_des",
											"rhs_uniform_msv_emr",
											"rhs_uniform_flora",
											"rhs_uniform_vdv_flora",
											"rhs_uniform_gorka_r_g",
											"rhs_uniform_gorka_r_y",
											"rhs_chdkz_uniform_5",
											"rhs_chdkz_uniform_4",
											"rhs_chdkz_uniform_3",
											"rhs_chdkz_uniform_2",
											"rhs_chdkz_uniform_1",
											"rhs_uniform_mvd_izlom",
											"rhs_uniform_cu_ocp_10th",
											"rhs_uniform_cu_ocp_1stcav",
											"rhs_uniform_cu_ocp_82nd",
											"rhs_uniform_cu_ucp_101st",
											"rhs_uniform_cu_ucp_10th",
											"rhs_uniform_cu_ucp_1stcav",
											"rhs_uniform_cu_ucp_82nd",
											"rhs_uniform_g3_m81",
											"rhs_uniform_g3_blk",
											"rhs_uniform_g3_mc",
											"rhs_uniform_g3_rgr",
											"rhs_uniform_g3_tan",
											"rhsgref_uniform_alpenflage",
											"rhsgref_uniform_flecktarn",
											"rhsgref_uniform_para_ttsko_mountain",
											"rhsgref_uniform_para_ttsko_oxblood",
											"rhsgref_uniform_para_ttsko_urban",
											"rhsgref_uniform_reed",
											"rhsgref_uniform_specter",
											"rhsgref_uniform_tigerstripe",
											"rhsgref_uniform_ttsko_forest",
											"rhsgref_uniform_ttsko_mountain",
											"rhsgref_uniform_ttsko_urban",
											"rhsgref_uniform_vsr",
											"rhsgref_uniform_woodland",
											"rhsgref_uniform_woodland_olive",
											"rhssaf_uniform_m10_digital",
											"rhssaf_uniform_m10_digital_summer",
											"rhssaf_uniform_m10_digital_desert",
											"rhssaf_uniform_m10_digital_tan_boots",
											"rhssaf_uniform_m93_oakleaf",
											"rhssaf_uniform_m93_oakleaf_summer",
											"rhssaf_uniform_heli_pilot",
											"rhs_uniform_FROG01_m81",
											"rhs_uniform_FROG01_d",
											"rhs_uniform_FROG01_wd",
											"rhs_uniform_cu_ocp",
											"rhs_uniform_cu_ucp",
											"rhs_uniform_cu_ocp_101st",
											"rhs_uniform_cu_ocp_10th",
											"rhs_uniform_cu_ocp_1stcav",
											"rhs_uniform_cu_ocp_82nd",
											"rhs_uniform_cu_ucp_101st",
											"rhs_uniform_cu_ucp_10th",
											"rhs_uniform_cu_ucp_1stcav",
											"rhs_uniform_cu_ucp_82nd",
											"rhs_uniform_cu_ocp_patchless",
											"rhs_uniform_cu_ucp_patchless",
											"rhs_uniform_g3_m81",
											"rhs_uniform_g3_blk",
											"rhs_uniform_g3_mc",
											"rhs_uniform_g3_rgr",
											"rhs_uniform_g3_tan"
										];
	DMS_assault_vests =					[							// Vests for Assault Class
											#ifdef GIVE_AI_APEX_GEAR
											"V_TacChestrig_grn_F",
											"V_PlateCarrier2_tna_F",
											"V_PlateCarrierSpec_tna_F",
											"V_PlateCarrierGL_tna_F",
											"V_TacVest_gen_F",
											"V_PlateCarrier1_rgr_noflag_F",
											#endif
											"V_PlateCarrierH_CTRG",
											"V_PlateCarrierSpec_rgr",
											"V_PlateCarrierGL_blk",
											"V_PlateCarrierGL_mtp",
											"V_PlateCarrierGL_rgr",
											"V_PlateCarrierSpec_blk",
											"V_PlateCarrierSpec_mtp",
											"V_PlateCarrierL_CTRG",
											"V_TacVest_blk_POLICE",
											"V_PlateCarrierIA2_dgtl",
											"HAP_VEST_KERRY_black",
											"HAP_VEST_KERRY_blue",
											"HAP_VEST_KERRY_brown",
											"HAP_VEST_KERRY_camo1_orange",
											"HAP_VEST_KERRY_camo2_whitegreen",
											"HAP_VEST_KERRY_camo2_woodland",
											"HAP_VEST_KERRY_camo3_woodland",
											"HAP_VEST_KERRY_red",
											"HAP_VEST_KERRY_white",
											"HAP_VEST_PRESS_blue",
											"HAP_VEST_PRESS_camo1_blue",
											"HAP_VEST_PRESS_camo1_wood1",
											"HAP_VEST_PRESS_camo2_orange",
											"HAP_VEST_PRESS_camo2_red",
											"HAP_VEST_PRESS_camo2_urban1",
											"HAP_VEST_PRESS_camo2_winter1",
											"HAP_VEST_PRESS_camo3_blue",
											"HAP_VEST_PRESS_camo3_wood1",
											"HAP_VEST_PRESS_green",
											"HAP_VEST_PRESS_herb1",
											"HAP_VEST_PRESS_jolly_roger",
											"HAP_VEST_PRESS_orange",
											"HAP_VEST_PRESS_pink",
											"HAP_VEST_PRESS_red",
											"HAP_VEST_TAC_HUNTER_1",
											"HAP_VEST_TAC_HUNTER_2",
											"HAP_VEST_TAC_HUNTER_3",
											"HAP_VEST_TAC_HUNTER_4",
											"HAP_VEST_TAC_HUNTER_5",
											"HAP_V_PlateCarrierGL_SOA",
											"HAP_V_PlateCarrierGL_blue",
											"HAP_V_PlateCarrierGL_brown",
											"HAP_V_PlateCarrierGL_camo1",
											"HAP_V_PlateCarrierGL_camo2",
											"HAP_V_PlateCarrierGL_camo3",
											"HAP_V_PlateCarrierGL_camo4",
											"HAP_V_PlateCarrierGL_green",
											"HAP_V_PlateCarrierGL_leo1",
											"HAP_V_PlateCarrierGL_red",
											"HAP_V_PlateCarrierGL_tiger",
											"HAP_press_vest_swat",
											"HAP_press_vest_tiger",
											"HAP_press_vest_winter1",
											"HAP_press_vest_winter2",
											"CUP_V_BAF_Osprey_Mk2_DDPM_Grenadier",
											"CUP_V_BAF_Osprey_Mk2_DDPM_Medic",
											"CUP_V_BAF_Osprey_Mk2_DDPM_Officer",
											"CUP_V_BAF_Osprey_Mk2_DDPM_Sapper",
											"CUP_V_BAF_Osprey_Mk2_DDPM_Scout",
											"CUP_V_BAF_Osprey_Mk2_DDPM_Soldier1",
											"CUP_V_BAF_Osprey_Mk2_DDPM_Soldier2",
											"CUP_V_BAF_Osprey_Mk2_DPM_Grenadier",
											"CUP_V_BAF_Osprey_Mk2_DPM_Medic",
											"CUP_V_BAF_Osprey_Mk2_DPM_Officer",
											"CUP_V_BAF_Osprey_Mk2_DPM_Sapper",
											"CUP_V_BAF_Osprey_Mk2_DPM_Scout",
											"CUP_V_BAF_Osprey_Mk2_DPM_Soldier1",
											"CUP_V_BAF_Osprey_Mk2_DPM_Soldier2",
											"CUP_V_BAF_Osprey_Mk4_MTP_Grenadier",
											"CUP_V_BAF_Osprey_Mk4_MTP_MachineGunner",
											"CUP_V_BAF_Osprey_Mk4_MTP_Rifleman",
											"CUP_V_BAF_Osprey_Mk4_MTP_SquadLeader",
											"CUP_V_B_GER_Carrier_Rig",
											"CUP_V_B_GER_Carrier_Rig_2",
											"CUP_V_B_GER_Carrier_Vest",
											"CUP_V_B_GER_Carrier_Vest_2",
											"CUP_V_B_GER_Carrier_Vest_3",
											"CUP_V_B_GER_Vest_1",
											"CUP_V_B_GER_Vest_2",
											"CUP_V_B_LHDVest_Blue",
											"CUP_V_B_LHDVest_Brown",
											"CUP_V_B_LHDVest_Green",
											"CUP_V_B_LHDVest_Red",
											"CUP_V_B_LHDVest_Violet",
											"CUP_V_B_LHDVest_White",
											"CUP_V_B_LHDVest_Yellow",
											"CUP_V_B_MTV",
											"CUP_V_B_MTV_LegPouch",
											"CUP_V_B_MTV_MG",
											"CUP_V_B_MTV_Marksman",
											"CUP_V_B_MTV_Mine",
											"CUP_V_B_MTV_Patrol",
											"CUP_V_B_MTV_PistolBlack",
											"CUP_V_B_MTV_Pouches",
											"CUP_V_B_MTV_TL",
											"CUP_V_B_MTV_noCB",
											"CUP_V_B_PilotVest",
											"CUP_V_B_RRV_DA1",
											"CUP_V_B_RRV_DA2",
											"CUP_V_B_RRV_Light",
											"CUP_V_B_RRV_MG",
											"CUP_V_B_RRV_Medic",
											"CUP_V_B_RRV_Officer",
											"CUP_V_B_RRV_Scout",
											"CUP_V_B_RRV_Scout2",
											"CUP_V_B_RRV_Scout3",
											"CUP_V_B_RRV_TL",
											"CUP_V_I_Carrier_Belt",
											"CUP_V_I_Guerilla_Jacket",
											"CUP_V_I_RACS_Carrier_Vest",
											"CUP_V_I_RACS_Carrier_Vest_2",
											"CUP_V_I_RACS_Carrier_Vest_3",
											"CUP_V_OI_TKI_Jacket1_01",
											"CUP_V_OI_TKI_Jacket1_02",
											"CUP_V_OI_TKI_Jacket1_03",
											"CUP_V_OI_TKI_Jacket1_04",
											"CUP_V_OI_TKI_Jacket1_05",
											"CUP_V_OI_TKI_Jacket1_06",
											"CUP_V_OI_TKI_Jacket2_01",
											"CUP_V_OI_TKI_Jacket2_02",
											"CUP_V_OI_TKI_Jacket2_03",
											"CUP_V_OI_TKI_Jacket2_04",
											"CUP_V_OI_TKI_Jacket2_05",
											"CUP_V_OI_TKI_Jacket2_06",
											"CUP_V_OI_TKI_Jacket3_01",
											"CUP_V_OI_TKI_Jacket3_02",
											"CUP_V_OI_TKI_Jacket3_03",
											"CUP_V_OI_TKI_Jacket3_04",
											"CUP_V_OI_TKI_Jacket3_05",
											"CUP_V_OI_TKI_Jacket3_06",
											"CUP_V_OI_TKI_Jacket4_01",
											"CUP_V_OI_TKI_Jacket4_02",
											"CUP_V_OI_TKI_Jacket4_03",
											"CUP_V_OI_TKI_Jacket4_04",
											"CUP_V_OI_TKI_Jacket4_05",
											"CUP_V_OI_TKI_Jacket4_06",
											"CUP_V_O_SLA_Carrier_Belt",
											"CUP_V_O_SLA_Carrier_Belt02",
											"CUP_V_O_SLA_Carrier_Belt03",
											"CUP_V_O_SLA_Flak_Vest01",
											"CUP_V_O_SLA_Flak_Vest02",
											"CUP_V_O_SLA_Flak_Vest03",
											"CUP_V_O_TK_CrewBelt",
											"CUP_V_O_TK_OfficerBelt",
											"CUP_V_O_TK_OfficerBelt2",
											"CUP_V_O_TK_Vest_1",
											"CUP_V_O_TK_Vest_2",
											"rhs_6sh92",
											"rhs_6sh92_radio",
											"rhs_6sh92_vog",
											"rhs_6sh92_vog_headset",
											"rhs_6sh92_headset",
											"rhs_6sh92_digi",
											"rhs_6sh92_digi_radio",
											"rhs_6sh92_digi_vog",
											"rhs_6sh92_digi_vog_headset",
											"rhs_6sh92_digi_headset",
											"rhs_6b23",
											"rhs_6b23_crew",
											"rhs_6b23_crewofficer",
											"rhs_6b23_engineer",
											"rhs_6b23_medic",
											"rhs_6b23_rifleman",
											"rhs_6b23_sniper",
											"rhs_6b23_6sh92",
											"rhs_6b23_6sh92_radio",
											"rhs_6b23_6sh92_vog",
											"rhs_6b23_6sh92_vog_headset",
											"rhs_6b23_6sh92_headset",
											"rhs_6b23_6sh92_headset_mapcase",
											"rhs_6b23_digi",
											"rhs_6b23_digi_crew",
											"rhs_6b23_digi_crewofficer",
											"rhs_6b23_digi_engineer",
											"rhs_6b23_digi_medic",
											"rhs_6b23_digi_rifleman",
											"rhs_6b23_digi_sniper",
											"rhs_6b23_digi_6sh92",
											"rhs_6b23_digi_6sh92_radio",
											"rhs_6b23_digi_6sh92_vog",
											"rhs_6b23_digi_6sh92_vog_headset",
											"rhs_6b23_digi_6sh92_headset",
											"rhs_6b23_digi_6sh92_headset_mapcase",
											"rhs_6b23_ML",
											"rhs_6b23_ML_crew",
											"rhs_6b23_ML_crewofficer",
											"rhs_6b23_ML_engineer",
											"rhs_6b23_ML_medic",
											"rhs_6b23_ML_rifleman",
											"rhs_6b23_ML_sniper",
											"rhs_6b23_ML_6sh92",
											"rhs_6b23_ML_6sh92_radio",
											"rhs_6b23_ML_6sh92_vog",
											"rhs_6b23_ML_6sh92_vog_headset",
											"rhs_6b23_ML_6sh92_headset",
											"rhs_6b23_ML_6sh92_headset_mapcase",
											"rhs_vest_commander",
											"rhs_vydra_3m",
											"rhsusf_iotv_ucp",
											"rhsusf_iotv_ucp_grenadier",
											"rhsusf_iotv_ucp_medic",
											"rhsusf_iotv_ucp_repair",
											"rhsusf_iotv_ucp_rifleman",
											"rhsusf_iotv_ucp_SAW",
											"rhsusf_iotv_ucp_squadleader",
											"rhsusf_iotv_ucp_teamleader",
											"rhsusf_iotv_ocp",
											"rhsusf_iotv_ocp_grenadier",
											"rhsusf_iotv_ocp_medic",
											"rhsusf_iotv_ocp_repair",
											"rhsusf_iotv_ocp_rifleman",
											"rhsusf_iotv_ocp_SAW",
											"rhsusf_iotv_ocp_squadleader",
											"rhsusf_iotv_ocp_teamleader",
											"rhsusf_spc",
											"rhsusf_spc_corpsman",
											"rhsusf_spc_patchless",
											"rhsusf_spc_squadleader",
											"rhsusf_spc_teamleader",
											"rhsusf_spc_light",
											"rhsusf_spc_rifleman",
											"rhsusf_spc_iar",
											"rhsusf_spcs_ocp_rifleman",
											"rhsusf_spcs_ocp",
											"rhsusf_spcs_ucp_rifleman",
											"rhsusf_spcs_ucp",
											"rhs_6b43",
											"rhsgref_6b23",
											"rhsgref_6b23_khaki",
											"rhsgref_6b23_khaki_medic",
											"rhsgref_6b23_khaki_nco",
											"rhsgref_6b23_khaki_officer",
											"rhsgref_6b23_khaki_rifleman",
											"rhsgref_6b23_khaki_sniper",
											"rhsgref_6b23_ttsko_digi",
											"rhsgref_6b23_ttsko_digi_medic",
											"rhsgref_6b23_ttsko_digi_nco",
											"rhsgref_6b23_ttsko_digi_officer",
											"rhsgref_6b23_ttsko_digi_rifleman",
											"rhsgref_6b23_ttsko_digi_sniper",
											"rhsgref_6b23_ttsko_forest",
											"rhsgref_6b23_ttsko_forest_rifleman",
											"rhsgref_6b23_ttsko_mountain",
											"rhsgref_6b23_ttsko_mountain_medic",
											"rhsgref_6b23_ttsko_mountain_nco",
											"rhsgref_6b23_ttsko_mountain_officer",
											"rhsgref_6b23_ttsko_mountain_rifleman",
											"rhsgref_6b23_ttsko_mountain_sniper",
											"rhsgref_otv_digi",
											"rhsgref_otv_khaki",
											"rhssaf_vest_md98_woodland",
											"rhssaf_vest_md98_md2camo",
											"rhssaf_vest_md98_digital",
											"rhssaf_vest_md98_officer",
											"rhssaf_vest_md98_rifleman",
											"rhssaf_vest_otv_md2camo",
											"rhssaf_vest_md99_md2camo_rifleman",
											"rhssaf_vest_md99_digital_rifleman",
											"rhssaf_vest_md99_woodland_rifleman",
											"rhssaf_vest_md99_md2camo",
											"rhssaf_vest_md99_digital",
											"rhssaf_vest_md99_woodland",
											"rhsusf_iotv_ucp",
											"rhsusf_iotv_ucp_grenadier",
											"rhsusf_iotv_ucp_medic",
											"rhsusf_iotv_ucp_repair",
											"rhsusf_iotv_ucp_rifleman",
											"rhsusf_iotv_ucp_SAW",
											"rhsusf_iotv_ucp_squadleader",
											"rhsusf_iotv_ucp_teamleader",
											"rhsusf_iotv_ocp",
											"rhsusf_iotv_ocp_grenadier",
											"rhsusf_iotv_ocp_medic",
											"rhsusf_iotv_ocp_repair",
											"rhsusf_iotv_ocp_rifleman",
											"rhsusf_iotv_ocp_SAW",
											"rhsusf_iotv_ocp_squadleader",
											"rhsusf_iotv_ocp_teamleader",
											"rhsusf_spc",
											"rhsusf_spc_mg",
											"rhsusf_spc_marksman",
											"rhsusf_spc_corpsman",
											"rhsusf_spc_patchless",
											"rhsusf_spc_squadleader",
											"rhsusf_spc_teamleader",
											"rhsusf_spc_light",
											"rhsusf_spc_rifleman",
											"rhsusf_spc_iar",
											"rhsusf_spcs_ocp_rifleman",
											"rhsusf_spcs_ocp",
											"rhsusf_spcs_ucp_rifleman",
											"rhsusf_spcs_ucp"
										];
	DMS_assault_backpacks =				[							// Backpacks for Assault Class
											#ifdef GIVE_AI_APEX_GEAR
											"B_Bergen_tna_F",
											"B_FieldPack_ghex_F",
											"B_ViperLightHarness_khk_F",
											#endif
											"B_Bergen_rgr",
											"B_Carryall_oli",
											"B_Kitbag_mcamo",
											"B_Carryall_cbr",
											"B_FieldPack_oucamo",
											"B_FieldPack_cbr",
											"B_Bergen_blk",
											"HAP_B_Bergen_blue",
											"HAP_B_Bergen_camo_blue",
											"HAP_B_Bergen_gooch",
											"HAP_B_Bergen_ice_camo_black",
											"HAP_B_Bergen_red",
											"HAP_B_Bergen_stars",
											"HAP_B_Bergen_weed_1",
											"HAP_B_Bergen_weed_2",
											"HAP_B_Bergen_white",
											"HAP_B_Bergen_woodland",
											"HAP_B_Bergen_woodland_2",
											"HAP_B_Carryall_black",
											"HAP_B_Carryall_desert",
											"HAP_B_Carryall_grnNbrwn",
											"HAP_B_Carryall_hazmat",
											"HAP_B_Carryall_orngNwhite",
											"HAP_B_Carryall_tiedye",
											"HAP_B_Carryall_whiteNgry",
											"HAP_B_Carryall_woodNblu",
											"HAP_B_Carryall_woodland",
											"HAP_B_Gorod_blue",
											"HAP_B_Gorod_explorer",
											"HAP_B_Gorod_floral_black",
											"HAP_B_Gorod_green",
											"HAP_B_Gorod_ice_camo_1",
											"HAP_B_Gorod_orange",
											"HAP_B_Gorod_white",
											"HAP_B_Gorod_winter_1",
											"CUP_B_ACRPara_m95",
											"CUP_B_AssaultPack_ACU",
											"CUP_B_AssaultPack_Black",
											"CUP_B_AssaultPack_Coyote",
											"CUP_B_Bergen_BAF",
											"CUP_B_CivPack_WDL",
											"CUP_B_GER_Pack_Flecktarn",
											"CUP_B_GER_Pack_Tropentarn",
											"CUP_B_HikingPack_Civ",
											"CUP_B_MOLLE_WDL",
											"CUP_B_RUS_Backpack",
											"CUP_B_USMC_AssaultPack",
											"CUP_B_USMC_MOLLE",
											"CUP_B_USPack_Black",
											"CUP_B_USPack_Coyote",
											"rhsusf_assault_eagleaiii_coy",
											"rhsusf_assault_eagleaiii_ocp",
											"rhsusf_assault_eagleaiii_ucp",
											"rhsusf_falconii_coy",
											"rhsusf_falconii_mc",
											"rhsusf_falconii",
											"rhs_assault_umbts",
											"rhs_assault_umbts_engineer_empty",
											"rhs_medic_bag",
											"rhs_sidor",
											"rhs_rpg_empty",
											"rhssaf_Kitbag_base",
											"rhssaf_kitbag_md2camo",
											"rhssaf_kitbag_digital",
											"rhssaf_kitbag_smb",
											"rhsusf_assault_eagleaiii_coy",
											"rhsusf_assault_eagleaiii_ocp",
											"rhsusf_assault_eagleaiii_ucp",
											"rhsusf_falconii_coy",
											"rhsusf_falconii_mc",
											"rhsusf_falconii",
											"RHS_M2_Gun_Bag"
										];

	//Machine Gun Class
	DMS_MG_weps	=						[							// Machine Guns
											#ifdef GIVE_AI_MARKSMAN_DLC_WEAPONS
											"MMG_01_hex_F",
											"MMG_02_black_F",
											#endif

											#ifdef GIVE_AI_APEX_WEAPONS
											"LMG_03_F",
											#endif
											"LMG_Zafir_F",
											"LMG_Mk200_F",
											"arifle_MX_SW_Black_F",
											"Exile_Weapon_RPK",
											"Exile_Weapon_PKP"
										];
	DMS_MG_pistols =				[							// Pistols for Assault Class (Set to empty array if you don't want to give them any pistols)
											"hgun_ACPC2_F",
											"hgun_Rook40_F",
											"hgun_P07_F",
											"hgun_Pistol_heavy_01_F",
											"hgun_Pistol_heavy_02_F",
											"Exile_Weapon_Colt1911",
											"Exile_Weapon_Makarov",
											"Exile_Weapon_Taurus",
											"Exile_Weapon_TaurusGold"
										];
	DMS_MG_optics =						[							//	Optics for MG Class
											#ifdef GIVE_AI_APEX_GEAR
											"optic_ERCO_khk_F",
											"optic_DMS_ghex_F",
											"optic_Arco_blk_F",
											#endif
											"optic_Hamr",
											"optic_Aco",
											"optic_Holosight",
											"optic_MRCO"
										];
	DMS_MG_optic_chance					= 50;						// Percentage chance that an MG Class AI will get an optic
	DMS_MG_bipod_chance					= 90;						// Percentage chance that an MG Class AI will get a bipod
	DMS_MG_suppressor_chance			= 10;						// Percentage chance that an MG Class AI will get a suppressor

	DMS_MG_items =						[							// Items for MG Class AI (Loot stuff that goes in uniform/vest/backpack)
											"Exile_Item_InstaDoc",
											"Exile_Item_Catfood_Cooked",
											"Exile_Item_PlasticBottleFreshWater",
											"Exile_Item_CookingPot"
										];
	DMS_MG_equipment =					[							// Equipment for MG Class AI (stuff that goes in toolbelt slots)
											"Binocular"
										];
	DMS_MG_RandItemCount =				3;							// How many random items to add to the AI's inventory.
	DMS_MG_RandItems =					[							// The random items that will be added to the AI's inventory.
											"Exile_Item_EMRE",
											"Exile_Item_Surstromming_Cooked",
											"Exile_Item_PowerDrink",
											"Exile_Item_PlasticBottleCoffee",
											"Exile_Item_Vishpirin",
											"Exile_Item_Instadoc"
										];
	DMS_MG_helmets =					[							// Helmets for MG Class
											#ifdef GIVE_AI_APEX_GEAR
											"H_HelmetB_TI_tna_F",
											"H_HelmetB_Enh_tna_F",
											"H_HelmetSpecO_ghex_F",
											"H_HelmetLeaderO_ghex_F",
											"H_HelmetCrew_O_ghex_F",
											#endif
											"H_PilotHelmetHeli_I",
											"H_PilotHelmetHeli_O",
											"H_PilotHelmetFighter_I",
											"H_PilotHelmetFighter_O",
											"H_HelmetCrew_O",
											"H_CrewHelmetHeli_I",
											"H_HelmetSpecB_paint1",
											"H_HelmetIA_camo",
											"H_HelmetLeaderO_ocamo",
											"H_HelmetLeaderO_oucamo"
										];
	DMS_MG_clothes =					[							// Uniforms for MG Class
											#ifdef GIVE_AI_APEX_GEAR
											"U_B_T_Soldier_F",
											"U_B_T_Soldier_SL_F",
											"U_B_CTRG_Soldier_F",
											"U_O_V_Soldier_Viper_F",
											"U_I_C_Soldier_Bandit_2_F",
											"U_I_C_Soldier_Camo_F",
											"U_B_CTRG_Soldier_urb_1_F",
											#endif
											"U_O_CombatUniform_ocamo",
											"U_O_PilotCoveralls",
											//"U_B_Wetsuit",
											"U_BG_Guerilla3_1",
											"U_BG_Guerilla2_3",
											"U_BG_Guerilla2_2",
											"U_BG_Guerilla1_1",
											"U_BG_Guerrilla_6_1",
											"U_IG_Guerilla3_2",
											"U_B_SpecopsUniform_sgg",
											"U_I_OfficerUniform",
											"U_B_CTRG_3",
											"U_I_G_resistanceLeader_F"
										];
	DMS_MG_vests =						[							// Vests for MG Class
											#ifdef GIVE_AI_APEX_GEAR
											"V_TacChestrig_grn_F",
											"V_PlateCarrier2_tna_F",
											"V_PlateCarrierSpec_tna_F",
											"V_PlateCarrierGL_tna_F",
											"V_TacVest_gen_F",
											"V_PlateCarrier1_rgr_noflag_F",
											#endif
											"V_PlateCarrierH_CTRG",
											"V_PlateCarrierSpec_rgr",
											"V_PlateCarrierGL_blk",
											"V_PlateCarrierGL_mtp",
											"V_PlateCarrierGL_rgr",
											"V_PlateCarrierSpec_blk",
											"V_PlateCarrierSpec_mtp",
											"V_PlateCarrierL_CTRG",
											"V_TacVest_blk_POLICE",
											"V_PlateCarrierIA2_dgtl",
											"V_HarnessO_brn",
											"V_HarnessO_gry"
										];
	DMS_MG_backpacks =					[							// Backpacks for MG Class
											#ifdef GIVE_AI_APEX_GEAR
											"B_Bergen_tna_F",
											"B_Carryall_ghex_F",
											"B_ViperHarness_ghex_F",
											"B_ViperLightHarness_ghex_F",
											#endif
											"B_Bergen_rgr",
											"B_Carryall_oli",
											"B_Kitbag_mcamo",
											"B_Carryall_cbr",
											"B_Bergen_blk"
										];

	//Sniper Class
	DMS_sniper_weps =					[							// Sniper Rifles
											"srifle_EBR_F",
											"srifle_GM6_F",
											"srifle_LRR_F",
											"arifle_MXM_Black_F",
											"srifle_DMR_01_F",
											#ifdef GIVE_AI_MARKSMAN_DLC_WEAPONS
											"srifle_DMR_02_F",
											"srifle_DMR_03_woodland_F",
											//"srifle_DMR_04_F",			// Does anybody like the ASP-1? :p
											"srifle_DMR_05_blk_F",
											"srifle_DMR_06_olive_F",
											#endif

											#ifdef GIVE_AI_APEX_WEAPONS
											"srifle_DMR_07_ghex_F",
											#endif
											"Exile_Weapon_DMR",
											"Exile_Weapon_SVD",
											"Exile_Weapon_VSSVintorez"
										];
	DMS_sniper_pistols =				[							// Pistols for Assault Class (Set to empty array if you don't want to give them any pistols)
											#ifdef GIVE_AI_APEX_WEAPONS
											"hgun_Pistol_01_F",
											#endif
											"hgun_ACPC2_F",
											"hgun_Rook40_F",
											"hgun_P07_F",
											"hgun_Pistol_heavy_01_F",
											"hgun_Pistol_heavy_02_F",
											"Exile_Weapon_Colt1911",
											"Exile_Weapon_Makarov",
											"Exile_Weapon_Taurus",
											"Exile_Weapon_TaurusGold"
										];
	DMS_sniper_optics =					[							// Optics for Sniper Class
											#ifdef GIVE_AI_APEX_GEAR
											"optic_SOS_khk_F",
											"optic_DMS_ghex_F",
											"optic_LRPS_tna_F",
											#endif

											#ifdef GIVE_AI_MARKSMAN_DLC_WEAPONS
											"optic_AMS_khk",
											#endif
											"optic_SOS",
											"optic_DMS",
											"optic_LRPS"
										];
	DMS_sniper_optic_chance				= 100;						// Percentage chance that a Sniper Class AI will get an optic
	DMS_sniper_bipod_chance				= 90;						// Percentage chance that a Sniper Class AI will get a bipod
	DMS_sniper_suppressor_chance		= 50;						// Percentage chance that a Sniper Class AI will get a suppressor

	DMS_sniper_items =					[							// Items for Sniper Class AI (Loot stuff that goes in uniform/vest/backpack)
											"Exile_Item_InstaDoc",
											"Exile_Item_Surstromming_Cooked",
											"Exile_Item_PlasticBottleFreshWater",
											"Exile_Item_PlasticBottleFreshWater",
											"Exile_Item_Matches"
										];
	DMS_sniper_equipment =				[							// Equipment for Sniper Class AI (stuff that goes in toolbelt slots)
											"Rangefinder",
											"ItemGPS"
										];
	DMS_sniper_RandItemCount =			3;							// How many random items to add to the AI's inventory.
	DMS_sniper_RandItems =				[							// The random items that will be added to the AI's inventory.
											"Exile_Item_EMRE",
											"Exile_Item_PlasticBottleCoffee",
											"Exile_Item_CanOpener",
											"Exile_Item_Instadoc",
											"Exile_Item_DuctTape"
										];
	DMS_sniper_helmets =				[							// Helmets for Sniper Class
											#ifdef GIVE_AI_APEX_GEAR
											//"H_HelmetO_ViperSP_ghex_F",			// Special helmet with in-built NVGs and thermal :o
											"H_HelmetB_Enh_tna_F",
											"H_HelmetSpecO_ghex_F",
											"H_HelmetLeaderO_ghex_F",
											#endif
											"H_HelmetSpecB_paint1",
											"H_HelmetIA_camo",
											"H_HelmetLeaderO_ocamo",
											"H_HelmetLeaderO_oucamo"
										];
	DMS_sniper_clothes =				[							// Uniforms for Sniper Class
											#ifdef GIVE_AI_APEX_GEAR
											"U_B_T_Sniper_F",
											"U_B_T_FullGhillie_tna_F",				// Invisible to thermal? 0_o
											"U_O_T_Sniper_F",
											"U_O_T_FullGhillie_tna_F",
											#endif
											"U_O_GhillieSuit",
											"U_B_FullGhillie_ard",
											"U_B_FullGhillie_lsh",
											"U_B_FullGhillie_sard",
											"U_B_GhillieSuit",
											"U_I_FullGhillie_ard",
											"U_I_FullGhillie_lsh",
											"U_I_FullGhillie_sard",
											"U_I_GhillieSuit",
											"U_O_FullGhillie_ard",
											"U_O_FullGhillie_lsh",
											"U_O_FullGhillie_sard"
										];
	DMS_sniper_vests =					[							// Vests for Sniper Class
											#ifdef GIVE_AI_APEX_GEAR
											"V_PlateCarrier2_tna_F",
											"V_PlateCarrierSpec_tna_F",
											"V_PlateCarrierGL_tna_F",
											"V_PlateCarrier2_rgr_noflag_F",
											#endif
											"V_PlateCarrierH_CTRG",
											"V_PlateCarrierSpec_rgr",
											"V_PlateCarrierGL_blk",
											"V_PlateCarrierGL_mtp",
											"V_PlateCarrierGL_rgr",
											"V_PlateCarrierSpec_blk",
											"V_PlateCarrierSpec_mtp",
											"V_PlateCarrierL_CTRG",
											"V_TacVest_blk_POLICE",
											"V_PlateCarrierIA2_dgtl",
											"V_HarnessO_brn",
											"V_HarnessO_gry"
										];
	DMS_sniper_backpacks =				[							// Backpacks for Sniper Class
											#ifdef GIVE_AI_APEX_GEAR
											"B_Bergen_tna_F",
											"B_Bergen_hex_F",
											"B_Carryall_ghex_F",
											"B_ViperHarness_ghex_F",
											"B_ViperHarness_blk_F",
											"B_ViperLightHarness_ghex_F",
											"B_ViperLightHarness_khk_F",
											#endif
											"B_Bergen_rgr",
											"B_Carryall_oli",
											"B_Kitbag_mcamo",
											"B_Carryall_cbr",
											"B_Bergen_blk"
										];

	DMS_ai_SupportedClasses =			[							// Allowed AI classes. If you want to create your own class, make sure you define everything as I've defined above, and add it here
											"assault",
											"MG",
											"sniper"
										];

	DMS_ai_SupportedRandomClasses = 	[							// Allowed "random" AI presets here if you want to create different random presets.
											"random",
											"random_non_assault",
											"random_non_MG",
											"random_non_sniper"
										];

	DMS_random_AI =						[							// Random AI preset that contains all default classes | DEFAULT: 60% Assault, 20% MG, 20% Sniper
											"assault",
											"assault",
											"assault",
											"MG",
											"sniper"
										];

	DMS_random_non_assault_AI =			[							// Random AI preset that excludes the "assault" class
											"MG",
											"MG",
											"sniper"
										];

	DMS_random_non_MG_AI =				[							// Random AI preset that excludes the "MG" class
											"assault",
											"assault",
											"sniper"
										];

	DMS_random_non_sniper_AI =			[							// Random AI preset that excludes the "sniper" class
											"assault",
											"assault",
											"MG"
										];

	DMS_ai_use_launchers				= true;						// Enable/disable spawning an AI in a group with a launcher
	DMS_ai_launchers_per_group			= 3;						// How many units per AI group can get a launcher.
	DMS_ai_use_launchers_chance			= 75;						// Percentage chance to actually spawn the launcher (per-unit). With "DMS_ai_launchers_per_group" set to 2, and "DMS_ai_use_launchers_chance" set to 50, there will be an average of 1 launcher per group.
	DMS_AI_launcher_ammo_count			= 2;						// How many rockets an AI will get with its launcher
	DMS_ai_remove_launchers				= false;						// Remove rocket launchers on AI death

	DMS_AI_wep_launchers_AT =			[							// AT Launchers
											#ifdef GIVE_AI_APEX_WEAPONS
											"launch_RPG7_F",
											#endif
											"launch_NLAW_F",
											"launch_RPG32_F",
											"launch_B_Titan_short_F"
										];
	DMS_AI_wep_launchers_AA =			[							// AA Launchers
											"launch_B_Titan_F"
										];

	DMS_RHeli_Height					= 500;						// Altitude of the heli when flying to drop point.
	DMS_RHeli_MinDistFromDrop			= 500;						// Minimum distance for the reinforcement heli to spawn from drop point.
	DMS_RHeli_MaxDistFromDrop			= 5000;						// Maximum distance for the reinforcement heli to spawn from drop point.
	DMS_RHeli_MinDistFromPlayers		= 1000;						// Minimum distance for the reinforcement heli to spawn from players.

/* AI Settings */


/* Loot Settings */
	DMS_GodmodeCrates 					= true;						// Whether or not crates will have godmode after being filled with loot.
	DMS_MinimumMagCount					= 3;						// Minimum number of magazines for weapons.
	DMS_MaximumMagCount					= 5;						// Maximum number of magazines for weapons.
	DMS_CrateCase_Sniper =				[							// If you pass "Sniper" in _lootValues, then it will spawn these weapons/items/backpacks
											[
												["Rangefinder",1],
												["srifle_GM6_F",1],
												["srifle_LRR_F",1],
												["srifle_EBR_F",1],
												["hgun_Pistol_heavy_01_F",1],
												["hgun_PDW2000_F",1]
											],
											[
												["ItemGPS",1],
												["U_B_FullGhillie_ard",1],
												["U_I_FullGhillie_lsh",1],
												["U_O_FullGhillie_sard",1],
												["U_O_GhillieSuit",1],
												["V_PlateCarrierGL_blk",1],
												["V_HarnessO_brn",1],
												["Exile_Item_InstaDoc",3],
												["Exile_Item_Surstromming_Cooked",5],
												["Exile_Item_PlasticBottleFreshWater",5],
												["optic_DMS",1],
												["acc_pointer_IR",1],
												["muzzle_snds_B",1],
												["optic_LRPS",1],
												["optic_MRD",1],
												["muzzle_snds_acp",1],
												["optic_Holosight_smg",1],
												["muzzle_snds_L",1],
												["5Rnd_127x108_APDS_Mag",3],
												["7Rnd_408_Mag",3],
												["20Rnd_762x51_Mag",5],
												["11Rnd_45ACP_Mag",3],
												["30Rnd_9x21_Mag",3]
											],
											[
												["B_Carryall_cbr",1],
												["B_Kitbag_mcamo",1]
											]
										];
	DMS_BoxWeapons =					[							// List of weapons that can spawn in a crate
											#ifdef USE_MARKSMAN_DLC_WEAPONS_IN_CRATES
											"srifle_DMR_02_F",
											"srifle_DMR_03_woodland_F",
											//"srifle_DMR_04_F",			// ASP-1 Kir
											"srifle_DMR_05_blk_F",
											"srifle_DMR_06_olive_F",
											"MMG_01_hex_F",
											"MMG_02_black_F",
											#endif

											#ifdef USE_APEX_WEAPONS_IN_CRATES
											"arifle_AK12_F",
											"arifle_ARX_ghex_F",
											"arifle_CTAR_blk_F",
											"arifle_SPAR_01_khk_F",
											"arifle_SPAR_03_khk_F",
											//"srifle_DMR_07_ghex_F",				// Oh great, a 6.5mm 20 round sniper rifle... because everybody wanted a nerfed MXM :p
											"LMG_03_F",
											#endif
											"Exile_Melee_Axe",
											"Exile_Melee_SledgeHammer",
											//"Exile_Melee_Shovel",					// Not really interesting for players...
											"arifle_Katiba_GL_F",
											"arifle_MX_GL_Black_F",
											"arifle_Mk20_GL_F",
											"arifle_TRG21_GL_F",
											"arifle_Katiba_F",
											"arifle_MX_Black_F",
											"arifle_TRG21_F",
											"arifle_TRG20_F",
											"arifle_Mk20_plain_F",
											"arifle_Mk20_F",
											"Exile_Weapon_AK107",
											"Exile_Weapon_AK107_GL",
											"Exile_Weapon_AK74_GL",
											"Exile_Weapon_AK47",
											"Exile_Weapon_AKS_Gold",
											"LMG_Zafir_F",
											"LMG_Mk200_F",
											"arifle_MX_SW_Black_F",
											"Exile_Weapon_RPK",
											"Exile_Weapon_PK",
											"Exile_Weapon_PKP",
											"srifle_EBR_F",
											"srifle_DMR_01_F",
											"srifle_GM6_F",
											"srifle_LRR_F",
											"arifle_MXM_Black_F",
											"Exile_Weapon_DMR",
											"Exile_Weapon_SVD",
											"Exile_Weapon_VSSVintorez",
											"Exile_Weapon_CZ550",
											"Exile_Weapon_SVDCamo",
											"HAP_CHEY_camo2_blue",
											"HAP_CHEY_camo2_woodland",
											"HAP_CHEY_camo3_black",
											"HAP_CHEY_camo3_green",
											"HAP_CHEY_camo3_pink",
											"HAP_CHEY_leo1",
											"HAP_CHEY_teal",
											"HAP_CHEY_white",
											"HAP_CHEY_woodland1",
											"HAP_CHEY_woodland1_green",
											"HAP_CHEY_woodland1_orange",
											"HAP_DMR_black",
											"HAP_DMR_camoORN",
											"HAP_DMR_digiGRN",
											"HAP_DMR_leo",
											"HAP_DMR_leoA",
											"HAP_DMR_snow",
											"HAP_DMR_zebra",
											"HAP_EBR_black",
											"HAP_EBR_desert",
											"HAP_EBR_desertA",
											"HAP_EBR_snow",
											"HAP_EBR_wavesblu",
											"HAP_LYNX_camo1_blue",
											"HAP_LYNX_camo1_green",
											"HAP_LYNX_camo1_tan",
											"HAP_LYNX_green",
											"HAP_LYNX_hex_beige",
											"HAP_LYNX_hex_blue",
											"HAP_LYNX_orange",
											"HAP_LYNX_red",
											"HAP_LYNX_white",
											"HAP_LYNX_yellow",
											"HAP_MARKS_ASP_blue",
											"HAP_MARKS_ASP_blueCamo",
											"HAP_MARKS_ASP_orangeCamo",
											"HAP_MARKS_ASP_snow",
											"HAP_MARKS_ASP_unicornPuke",
											"HAP_MARKS_ASP_white",
											"HAP_MARKS_CYRUS_brightRed",
											"HAP_MARKS_CYRUS_camo1",
											"HAP_MARKS_CYRUS_camo2",
											"HAP_MARKS_CYRUS_eyesOn1",
											"HAP_MARKS_CYRUS_herb1",
											"HAP_MARKS_CYRUS_herb2",
											"HAP_MARKS_CYRUS_herb3",
											"HAP_MARKS_CYRUS_limeGreen",
											"HAP_MARKS_CYRUS_orange1",
											"HAP_MARKS_CYRUS_red1",
											"HAP_MARKS_CYRUS_skulls1",
											"HAP_MARKS_CYRUS_skullsRed1",
											"HAP_MARKS_CYRUS_zebra1",
											"HAP_MARKS_DMR_02_camo1",
											"HAP_MARKS_DMR_02_camo2",
											"HAP_MARKS_DMR_02_camo3",
											"HAP_MARKS_DMR_02_herb1",
											"HAP_MARKS_DMR_02_herb2",
											"HAP_MARKS_DMR_02_herb3",
											"HAP_MARKS_DMR_02_rainbowsunshine1",
											"HAP_MARKS_DMR_02_squared1",
											"HAP_MARKS_EMR_camo1",
											"HAP_MARKS_EMR_camo2",
											"HAP_MARKS_EMR_purple1",
											"HAP_MARKS_EMR_rainbow1",
											"HAP_MARKS_EMR_red1",
											"HAP_MARKS_EMR_teal1",
											"HAP_MARKS_EMR_white1",
											"HAP_MARKS_EMR_whiteCamo1",
											"HAP_MARKS_M14_camo1",
											"HAP_MARKS_M14_camo2",
											"HAP_MARKS_M14_camo3",
											"HAP_MARKS_M14_flames1",
											"HAP_MARKS_M14_white",
											"HAP_MARKS_M14_wood1",
											"HAP_MX_camo1_green",
											"HAP_MX_camo1_purple",
											"HAP_MX_camo1_yellow",
											"HAP_MX_green",
											"HAP_MX_hex_beige",
											"HAP_MX_hex_blue",
											"HAP_MX_hex_green",
											"HAP_MX_pink",
											"HAP_MX_red",
											"HAP_MX_teal",
											"HAP_MX_yellow",
											"CUP_srifle_AWM_des",
											"CUP_srifle_AWM_wdl",
											"CUP_srifle_CZ750",
											"CUP_srifle_DMR",
											"CUP_srifle_CZ550",
											"CUP_srifle_LeeEnfield",
											"CUP_srifle_M14",
											"CUP_srifle_Mk12SPR",
											"CUP_srifle_M24_des",
											"CUP_srifle_M24_wdl",
											"CUP_srifle_M40A3",
											"CUP_srifle_M107_Base",
											"CUP_srifle_M110",
											"CUP_srifle_SVD",
											"CUP_srifle_SVD_des",
											"CUP_srifle_ksvk",
											"CUP_srifle_VSSVintorez",
											"CUP_srifle_AS50",
											"CUP_arifle_AK74",
											"CUP_arifle_AK107",
											"CUP_arifle_AK107_GL",
											"CUP_arifle_AKS74",
											"CUP_arifle_AKS74U",
											"CUP_arifle_AK74_GL",
											"CUP_arifle_AKM",
											"CUP_arifle_AKS",
											"CUP_arifle_AKS_Gold",
											"CUP_arifle_RPK74",
											"CUP_arifle_CZ805_A2",
											"CUP_arifle_FNFAL",
											"CUP_arifle_FNFAL_railed",
											"CUP_arifle_G36A",
											"CUP_arifle_G36A_camo",
											"CUP_arifle_G36K",
											"CUP_arifle_G36K_camo",
											"CUP_arifle_G36C",
											"CUP_arifle_G36C_camo",
											"CUP_arifle_MG36",
											"CUP_arifle_MG36_camo",
											"CUP_arifle_L85A2",
											"CUP_arifle_L85A2_GL",
											"CUP_arifle_L86A2",
											"CUP_arifle_M16A2",
											"CUP_arifle_M16A2_GL",
											"CUP_arifle_M16A4_GL",
											"CUP_arifle_M4A1",
											"CUP_arifle_M4A1_camo",
											"CUP_arifle_M16A4_Base",
											"CUP_arifle_M4A1_BUIS_GL",
											"CUP_arifle_M4A1_BUIS_camo_GL",
											"CUP_arifle_M4A1_BUIS_desert_GL",
											"CUP_arifle_M4A1_black",
											"CUP_arifle_M4A1_desert",
											"CUP_arifle_Sa58P",
											"CUP_arifle_Sa58V",
											"CUP_arifle_Mk16_CQC",
											"CUP_arifle_XM8_Compact_Rail",
											"CUP_arifle_XM8_Railed",
											"CUP_arifle_XM8_Carbine",
											"CUP_arifle_XM8_Carbine_FG",
											"CUP_arifle_XM8_Carbine_GL",
											"CUP_arifle_XM8_Compact",
											"CUP_arifle_xm8_SAW",
											"CUP_arifle_xm8_sharpshooter",
											"CUP_arifle_CZ805_A1",
											"CUP_arifle_CZ805_GL",
											"CUP_arifle_CZ805_B_GL",
											"CUP_arifle_CZ805_B",
											"CUP_arifle_Sa58P_des",
											"CUP_arifle_Sa58V_camo",
											"CUP_arifle_Sa58RIS1",
											"CUP_arifle_Sa58RIS2",
											"CUP_arifle_Sa58RIS2_camo",
											"CUP_arifle_Mk16_CQC_FG",
											"CUP_arifle_Mk16_CQC_SFG",
											"CUP_arifle_Mk16_CQC_EGLM",
											"CUP_arifle_Mk16_STD",
											"CUP_arifle_Mk16_STD_FG",
											"CUP_arifle_Mk16_STD_SFG",
											"CUP_arifle_Mk16_STD_EGLM",
											"CUP_arifle_Mk16_SV",
											"CUP_arifle_Mk17_CQC",
											"CUP_arifle_Mk17_CQC_FG",
											"CUP_arifle_Mk17_CQC_SFG",
											"CUP_arifle_Mk17_CQC_EGLM",
											"CUP_arifle_Mk17_STD",
											"CUP_arifle_Mk17_STD_FG",
											"CUP_arifle_Mk17_STD_SFG",
											"CUP_arifle_Mk17_STD_EGLM",
											"CUP_arifle_Mk20",
											"CUP_sgun_AA12",
											"CUP_sgun_M1014",
											"CUP_sgun_Saiga12K",
											"CUP_lmg_L7A2",
											"CUP_lmg_L110A1",
											"CUP_lmg_M240",
											"CUP_lmg_M249",
											"CUP_lmg_M249_para",
											"CUP_lmg_Mk48_des",
											"CUP_lmg_Mk48_wdl",
											"CUP_lmg_PKM",
											"CUP_lmg_UK59",
											"CUP_lmg_Pecheneg",
											"CUP_smg_bizon",
											"CUP_smg_EVO",
											"CUP_smg_MP5SD6",
											"CUP_smg_MP5A5",
											"rhs_weap_m249_pip_L",
											"rhs_weap_m249_pip_L_para",
											"rhs_weap_m249_pip_L_vfg",
											"rhs_weap_m249_pip_S",
											"rhs_weap_m249_pip_S_para",
											"rhs_weap_m249_pip_S_vfg",
											"rhs_weap_m240B",
											"rhs_weap_m240B_CAP",
											"rhs_weap_m240G",
											"rhs_weap_pkm",
											"rhs_weap_pkp",
											"rhs_weap_m27iar",
											"rhsusf_weap_MP7A2",
											"rhsusf_weap_MP7A2_desert",
											"rhsusf_weap_MP7A2_aor1",
											"rhsusf_weap_MP7A2_winter",
											"rhs_weap_hk416d10",
											"rhs_weap_hk416d10_LMT",
											"rhs_weap_hk416d10_m320",
											"rhs_weap_hk416d145",
											"rhs_weap_hk416d145_m320",
											"rhs_weap_m16a4",
											"rhs_weap_m16a4_carryhandle",
											"rhs_weap_m16a4_carryhandle_M203",
											"rhs_weap_m16a4_carryhandle_pmag",
											"rhs_weap_m4_carryhandle",
											"rhs_weap_m4_carryhandle_pmag",
											"rhs_weap_m4_m203",
											"rhs_weap_m4_m320",
											"rhs_weap_m4a1",
											"rhs_weap_m4a1_blockII",
											"rhs_weap_m4a1_blockII_KAC",
											"rhs_weap_m4a1_blockII_KAC_bk",
											"rhs_weap_m4a1_blockII_KAC_d",
											"rhs_weap_m4a1_blockII_KAC_wd",
											"rhs_weap_m4a1_blockII_M203",
											"rhs_weap_m4a1_blockII_M203_bk",
											"rhs_weap_m4a1_blockII_M203_d",
											"rhs_weap_m4a1_blockII_M203_wd",
											"rhs_weap_m4a1_blockII_bk",
											"rhs_weap_m4a1_blockII_d",
											"rhs_weap_m4a1_blockII_wd",
											"rhs_weap_m4a1_carryhandle",
											"rhs_weap_m4a1_carryhandle_m203",
											"rhs_weap_m4a1_carryhandle_pmag",
											"rhs_weap_m4a1_m203",
											"rhs_weap_m4a1_m320",
											"rhs_weap_mk18",
											"rhs_weap_mk18",
											"rhs_weap_mk18_KAC",
											"rhs_weap_mk18_KAC_bk",
											"rhs_weap_mk18_KAC_d",
											"rhs_weap_mk18_KAC_wd",
											"rhs_weap_mk18_bk",
											"rhs_weap_mk18_d",
											"rhs_weap_mk18_m320",
											"rhs_weap_mk18_wd",
											"rhs_weap_sr25",
											"rhs_weap_sr25_ec",
											"rhs_weap_m14ebrri",
											"rhs_weap_XM2010",
											"rhs_weap_XM2010_wd",
											"rhs_weap_XM2010_d",
											"rhs_weap_XM2010_sa",
											"rhs_weap_svd",
											"rhs_weap_svdp_wd",
											"rhs_weap_svdp_wd_npz",
											"rhs_weap_svdp_npz",
											"rhs_weap_svds",
											"rhs_weap_svds_npz",
											"rhs_weap_M107",
											"rhs_weap_M107_d",
											"rhs_weap_M107_w",
											"rhs_weap_t5000",
											"rhs_weap_m24sws",
											"rhs_weap_m24sws_blk",
											"rhs_weap_m24sws_ghillie",
											"rhs_weap_m40a5",
											"rhs_weap_m40a5_d",
											"rhs_weap_m40a5_w",
											"rhs_weap_sr25_d",
											"rhs_weap_sr25_wd",
											"rhs_weap_sr25_ec_d",
											"rhs_weap_sr25_ec_wd",
											"rhs_weap_kar98k",
											"rhs_weap_m21a",
											"rhs_weap_m21a_fold",
											"rhs_weap_m21a_pr",
											"rhs_weap_m21s",
											"rhs_weap_m21s_fold",
											"rhs_weap_m21s_pr",
											"rhs_weap_m38",
											"rhs_weap_m70ab2",
											"rhs_weap_m70ab2_fold",
											"rhs_weap_m70b1",
											"rhs_weap_m76",
											"rhs_weap_m92",
											"rhs_weap_m92_fold",
											"rhs_weap_m70ab2_fold",
											"rhs_weap_m70b1",
											"rhs_weap_m70b1n",
											"rhs_weap_m70b3n",
											"rhs_weap_m70b3n_pbg40",
											"rhs_weap_m92",
											"rhs_weap_m92_fold",
											"rhs_weap_m76",
											"rhs_weap_m21a",
											"rhs_weap_m21a_pr",
											"rhs_weap_m21a_pr_pbg40",
											"rhs_weap_m21a_fold",
											"rhs_weap_m21a_pbg40",
											"rhs_weap_m21s",
											"rhs_weap_m21s_pr",
											"rhs_weap_m21s_fold",
											"rhs_weap_m82a1",
											"rhs_weap_minimi_para_railed",
											"rhs_weap_g36c",
											"rhs_weap_g36kv",
											"rhs_weap_g36kv_ag36",
											"rhs_weap_m84",
											"rhs_weap_m249_pip_L",
											"rhs_weap_m249_pip_L_para",
											"rhs_weap_m249_pip_L_vfg",
											"rhs_weap_m249_pip_S",
											"rhs_weap_m249_pip_S_para",
											"rhs_weap_m249_pip_S_vfg",
											"rhs_weap_m240B",
											"rhs_weap_m240B_CAP",
											"rhs_weap_m240G",
											"rhsusf_weap_MP7A2",
											"rhsusf_weap_MP7A2_desert",
											"rhsusf_weap_MP7A2_aor1",
											"rhsusf_weap_MP7A2_winter",
											"rhs_weap_hk416d10",
											"rhs_weap_hk416d10_LMT",
											"rhs_weap_hk416d10_m320",
											"rhs_weap_hk416d145",
											"rhs_weap_hk416d145_m320",
											"rhs_weap_m16a4",
											"rhs_weap_m16a4_carryhandle",
											"rhs_weap_m16a4_carryhandle_M203",
											"rhs_weap_m16a4_carryhandle_pmag",
											"rhs_weap_m4",
											"rhs_weap_m4_carryhandle",
											"rhs_weap_m4_carryhandle_pmag",
											"rhs_weap_m4_m203",
											"rhs_weap_m4_m320",
											"rhs_weap_m4a1",
											"rhs_weap_m4a1_blockII",
											"rhs_weap_m4a1_blockII_KAC",
											"rhs_weap_m4a1_blockII_KAC_bk",
											"rhs_weap_m4a1_blockII_KAC_d",
											"rhs_weap_m4a1_blockII_KAC_wd",
											"rhs_weap_m4a1_blockII_M203",
											"rhs_weap_m4a1_blockII_M203_bk",
											"rhs_weap_m4a1_blockII_M203_d",
											"rhs_weap_m4a1_blockII_M203_wd",
											"rhs_weap_m4a1_blockII_bk",
											"rhs_weap_m4a1_blockII_d",
											"rhs_weap_m4a1_blockII_wd",
											"rhs_weap_m4a1_carryhandle",
											"rhs_weap_m4a1_carryhandle_m203",
											"rhs_weap_m4a1_carryhandle_pmag",
											"rhs_weap_m4a1_m203",
											"rhs_weap_m4a1_m320",
											"rhs_weap_mk18",
											"rhs_weap_mk18",
											"rhs_weap_mk18_KAC",
											"rhs_weap_mk18_KAC_bk",
											"rhs_weap_mk18_KAC_d",
											"rhs_weap_mk18_KAC_wd",
											"rhs_weap_mk18_bk",
											"rhs_weap_mk18_d",
											"rhs_weap_mk18_m320",
											"rhs_weap_mk18_wd"
										];
	DMS_BoxFood =						[							// List of food that can spawn in a crate.
											"Exile_Item_GloriousKnakworst_Cooked",
											"Exile_Item_Surstromming_Cooked",
											"Exile_Item_SausageGravy_Cooked",
											"Exile_Item_ChristmasTinner_Cooked",
											"Exile_Item_BBQSandwich_Cooked",
											"Exile_Item_Catfood_Cooked",
											"Exile_Item_DogFood_Cooked",
											"Exile_Item_EMRE",
											"Exile_Item_BeefParts",
											"Exile_Item_Noodles",
											"Exile_Item_SeedAstics",
											"Exile_Item_Raisins",
											"Exile_Item_Moobar",
											"Exile_Item_InstantCoffee"
										];
	DMS_BoxDrinks =						[
											"Exile_Item_PlasticBottleCoffee",
											"Exile_Item_PowerDrink",
											"Exile_Item_PlasticBottleFreshWater",
											"Exile_Item_EnergyDrink",
											"Exile_Item_MountainDupe",
											"Exile_Item_ChocolateMilk"
										];
	DMS_BoxMeds =						[
											"Exile_Item_InstaDoc",
											"Exile_Item_Vishpirin",
											"Exile_Item_Bandage"
										];
	DMS_BoxSurvivalSupplies	=			[							//List of survival supplies (food/drink/meds) that can spawn in a crate. "DMS_BoxFood", "DMS_BoxDrinks", and "DMS_BoxMeds" is automatically added to this list.
											"Exile_Item_Matches",
											"Exile_Item_CookingPot",
											"Exile_Melee_Axe",
											"Exile_Item_CanOpener"
										] + DMS_BoxFood + DMS_BoxDrinks + DMS_BoxMeds;
	DMS_Box_BaseParts_Wood =			[							// List of wooden base parts.
											"Exile_Item_WoodWallKit",
											"Exile_Item_WoodWallHalfKit",
											"Exile_Item_WoodWindowKit",
											"Exile_Item_WoodDoorKit",
											"Exile_Item_WoodDoorwayKit",
											"Exile_Item_WoodGateKit",
											"Exile_Item_WoodFloorKit",
											"Exile_Item_WoodFloorPortKit",
											"Exile_Item_WoodStairsKit"
										];
	DMS_Box_BaseParts_Concrete =		[							// List of concrete base parts
											"Exile_Item_ConcreteWallKit",
											"Exile_Item_ConcreteWindowKit",
											"Exile_Item_ConcreteDoorKit",
											"Exile_Item_ConcreteDoorwayKit",
											"Exile_Item_ConcreteGateKit",
											"Exile_Item_ConcreteFloorKit",
											"Exile_Item_ConcreteFloorPortKit",
											"Exile_Item_ConcreteStairsKit",
											"EBM_Brickwall_window_Kit",
											"EBM_Brickwall_stairs_Kit",
											"EBM_Brickwall_floorport_door_Kit",
											"EBM_Brickwall_floorport_Kit",
											"EBM_Brickwall_floor_Kit",
											"EBM_Brickwall_door_Kit",
											"EBM_Brickwall_hole_Kit",
											"EBM_Helipad_Kit",
											"EBM_Airhook_Kit",
											"EBM_Parksign_Kit",
											"EBM_Brickwall_Kit"
										];
	DMS_BoxBaseParts =					[							// List of all base parts to spawn. Weighted towards wood base parts.
											"Exile_Item_FortificationUpgrade",
											"Exile_Item_FortificationUpgrade",
											"Exile_Item_SandBagsKit_Long",
											"Exile_Item_SandBagsKit_Long",
											"Exile_Item_SandBagsKit_Corner",
											"Exile_Item_SandBagsKit_Corner",
											"Exile_Item_HBarrier5Kit",
											"EBM_Medikit_Kit",
											"EBM_pollard_Kit",
											"EBM_ATM_Kit",
											"Land_FlatTV_01_F_Kit",
											"Land_ChairPlastic_F_Kit",
											"Land_GamingSet_01_console_F_Kit",
											"Land_GamingSet_01_controller_F_Kit",
											"Land_GymBench_01_F_Kit",
											"Land_GymRack_03_F_Kit",
											"Land_OfficeCabinet_01_F_Kit",
											"Land_OfficeChair_01_F_Kit",
											"Land_PCSet_01_case_F_Kit",
											"Land_PCSet_01_keyboard_F_Kit",
											"Land_PCSet_01_mouse_F_Kit",
											"Land_PCSet_01_screen_F_Kit",
											"Land_Printer_01_F_Kit",
											"Land_RattanChair_01_F_Kit",
											"Land_RattanTable_01_F_Kit",
											"Land_Sleeping_bag_blue_F_Kit",
											"Land_Sleeping_bag_brown_F_Kit",
											"Land_Trophy_01_bronze_F_Kit",
											"Land_Trophy_01_gold_F_Kit",
											"Land_Trophy_01_silver_F_Kit",
											"Land_Sun_chair_green_F_Kit",
											"Land_Sunshade_01_F_Kit",
											"Land_Sunshade_02_F_Kit",
											"Land_Sunshade_03_F_Kit",
											"Land_Sunshade_F_Kit",
											"Land_TablePlastic_01_F_Kit",
											"Land_WoodenTable_large_F_Kit",
											"Land_WoodenTable_small_F_Kit",
											"OfficeTable_01_new_F_Kit",
											"Land_DieselGroundPowerUnit_01_F_Kit",
											"Land_EngineCrane_01_F_Kit",
											"Land_PalletTrolley_01_yellow_F_Kit",
											"Land_PressureWasher_01_F_Kit",
											"Land_WeldingTrolley_01_F_Kit",
											"Land_Workbench_01_F_Kit",
											"ArrowDesk_L_F_Kit",
											"ArrowDesk_R_F_Kit",
											"PlasticBarrier_02_grey_F_Kit",
											"PlasticBarrier_02_yellow_F_Kit",
											"PlasticBarrier_03_blue_F_Kit",
											"PlasticBarrier_03_orange_F_Kit",
											"RoadBarrier_F_Kit",
											"RoadBarrier_small_F_Kit",
											"RoadCone_F_Kit",
											"RoadCone_L_F_Kit",
											"TapeSign_F_Kit",
											"Land_Target_Dueling_01_F_Kit",
											"TargetP_Inf_F_Kit",
											"Fridge_01_closed_F_Kit",
											"Land_MetalCase_01_large_F_Kit",
											"Land_Microwave_01_F_Kit",
											"Land_ShelvesWooden_F_Kit",
											"Land_ShelvesWooden_blue_F_Kit",
											"Land_ShelvesWooden_khaki_F_Kit",
											"Land_ToolTrolley_01_F_Kit",
											"Land_ToolTrolley_02_F_Kit",
											"Land_Sign_Mines_F_Kit",
											"Land_PortableHelipadLight_01_F_Kit",
											"PortableHelipadLight_01_blue_F_Kit",
											"PortableHelipadLight_01_green_F_Kit",
											"PortableHelipadLight_01_red_F_Kit",
											"PortableHelipadLight_01_white_F_Kit",
											"PortableHelipadLight_01_yellow_F_Kit",
											"Land_GamingSet_01_powerSupply_F_Kit",
											"Land_GamingSet_01_camera_F_Kit",
											"WaterPump_01_forest_F_Kit",
											"WaterPump_01_sand_F_Kit",
											"Land_TripodScreen_01_large_F_Kit",
											"Land_TripodScreen_01_dual_v2_F_Kit",
											"Land_TripodScreen_01_dual_v1_F_Kit",
											"TargetP_Inf_Acc2_F_Kit",
											"TargetBootcampHuman_F_Kit",
											"Target_F_Kit",
											"Land_SatelliteAntenna_01_F_Kit",
											"Land_Projector_01_F_Kit",
											"Land_PortableGenerator_01_F_Kit",
											"Land_Obstacle_Ramp_F_Kit",
											"MetalBarrel_burning_F_Kit",
											"Land_i_House_Small_03_V1_F_Kit",
											"Land_i_House_Big_01_V2_F_Kit",
											"Land_PlasticCase_01_medium_F_Kit",
											"Land_Research_HQ_F_Kit",
											"Land_Research_house_V1_F_Kit",
											"Land_Suitcase_F_Kit",
											"B_Slingload_01_fuel_F_Kit",
											"B_Slingload_01_Ammo_F_Kit",
											"Exile_ConcreteMixer_Kit",
											"Flag_CSAT_F_Kit",
											"Land_GarbageContainer_closed_F_Kit",
											"Land_Metal_rack_F_Kit",
											"Land_Sink_F_Kit",
											"Land_CampingChair_V2_F_Kit",
											"Land_CampingChair_V1_F_Kit",
											"Land_Camping_Light_F_Kit",
											"Land_CampingTable_F_Kit",
											"MapBoard_altis_F_Kit",
											"Land_Pavement_narrow_F_Kit",
											"Land_Pavement_narrow_corner_F_Kit",
											"Land_Pavement_wide_F_Kit",
											"Land_Pavement_wide_corner_F_Kit",
											"Land_SharpStone_01_F_Kit",
											"Land_SharpStone_02_F_Kit",
											"Land_Sleeping_bag_F_Kit",
											"Land_Small_Stone_02_F_Kit",
											"Land_SolarPanel_2_F_Kit",
											"Land_spp_Panel_F_Kit",
											"Land_Airport_Tower_F_Kit",
											"Land_i_Barracks_V1_F_Kit",
											"Land_BeachBooth_01_F_Kit",
											"Land_Castle_01_tower_F_Kit",
											"Land_Sign_WarningUnexplodedAmmo_F_Kit",
											"Land_TTowerSmall_1_F_Kit",
											"Land_Dome_Big_F_Kit",
											"Land_Hangar_F_Kit",
											"Land_Metal_Shed_F_Kit",
											"Land_spp_Tower_F_Kit",
											"Land_Sun_chair_F_Kit",
											"Land_Sunshade_04_F_Kit",
											"Land_LampShabby_F_Kit",
											"Land_HBarrier_1_F_Kit",
											"Land_HBarrier_3_F_Kit",
											"Land_HBarrier_5_F_Kit",
											"Land_BagBunker_Tower_F_Kit",
											"Land_CncBarrier_F_Kit",
											"Land_Stone_4m_F_Kit",
											"Land_Stone_Gate_F_Kit",
											"PierLadder_F_Kit",
											"Land_WaterCooler_01_new_F_Kit",
											"Land_Pallet_MilBoxes_F_Kit",
											"Land_Cargo20_military_green_F_Kit",
											"Land_BagFence_Corner_F_Kit",
											"Land_BagFence_Long_F_Kit",
											"Land_HBarrierTower_F_Kit",
											"Land_Metal_wooden_rack_F_Kit",
											"Land_BagBunker_Small_F_Kit",
											"Land_Bunker_F_Kit",
											"Land_HBarrierWall_corner_F_Kit",
											"Land_HBarrierWall6_F_Kit",
											"Land_HBarrierWall4_F_Kit",
											"Land_RampConcrete_F_Kit",
											"Land_Wall_IndCnc_4_F_Kit",
											"Land_City2_4m_F_Kit",
											"Land_City2_8m_F_Kit",
											"Land_HelipadCivil_F_Kit",
											"Land_Rack_F_Kit",
											"Land_City_Gate_F_Kit",
											"Land_BarGate_F_Kit",
											"Land_Icebox_F_Kit",
											"Land_Sign_WarningMilitaryArea_F_Kit",
											"Land_Mil_WallBig_4m_F_Kit",
											"Land_Cargo_Patrol_V2_F_Kit",
											"Land_Pier_small_F_Kit",
											"Land_Wall_Tin_4_Kit",
											"Land_Tank_rust_F_Kit",
											"Land_ShelvesMetal_F_Kit",
											"Land_Cargo_Tower_V2_F_Kit",
											"Land_FuelStation_Feed_F_Kit",
											"Land_BagBunker_Large_F_Kit",
											"ShootingPos_F_Kit",
											"Land_cargo_house_slum_F_Kit",
											"Land_Cargo40_light_green_F_Kit",
											"Land_Cargo_House_V2_F_Kit",
											"Land_LampHalogen_F_Kit",
											"Land_LampAirport_F_Kit",
											"Land_CncShelter_F_Kit",
											"Land_Wall_IndCnc_2deco_F_Kit",
											"Land_CncWall4_F_Kit",
											"Land_FuelStation_Shed_F_Kit",
											"Land_Shed_Small_F_Kit",
											"Land_Razorwire_F_Kit",
											"Land_Cargo20_sand_F_Kit",
											"Land_GH_Stairs_F_Kit",
											"Land_i_Garage_V2_F_Kit",
											"Land_GH_Platform_F_Kit",
											"Land_TentDome_F_Kit",
											"Land_TentHangar_V1_F_Kit",
											"Land_CncWall1_F_Kit",
											"Land_CncBarrierMedium_F_Kit",
											"Land_Crash_barrier_F_Kit",
											"Land_Shed_Big_F_Kit",
											"Land_TouristShelter_01_F_Kit",
											"Land_Water_source_F_Kit",
											"Land_Sign_WarningMilitaryVehicles_F_Kit",
											"Land_Sign_WarningMilAreaSmall_F_Kit",
											"Land_Concrete_SmallWall_8m_F_Kit",
											"Land_Concrete_SmallWall_4m_F_Kit",
											"Land_PortableLight_double_F_Kit",
											"Land_Radar_Small_F_Kit",
											"Land_Cargo_addon02_V2_F_Kit",
											"Land_TableDesk_F_Kit",
											"Land_ToiletBox_F_Kit",
											"Land_ChairWood_F_Kit",
											"BlockConcrete_F_Kit",
											"Land_CargoBox_V1_F_Kit",
											"Land_IndFnc_3_F_Kit",
											"Land_IndFnc_9_F_Kit",
											"Land_Sea_Wall_F_Kit",
											"Land_i_Addon_03_V1_F_Kit",
											"Land_i_Addon_03mid_V1_F_Kit",
											"Land_LampStreet_F_Kit"
										] + DMS_Box_BaseParts_Wood + DMS_Box_BaseParts_Wood + DMS_Box_BaseParts_Wood + DMS_Box_BaseParts_Concrete;
	DMS_BoxCraftingMaterials =			[
											"Exile_Item_Cement",
											"Exile_Item_Sand",
											"Exile_Item_Sand",
											"Exile_Item_WaterCanisterDirtyWater",
											"Exile_Item_MetalBoard",
											"Exile_Item_MetalPole",
											"Exile_Item_MetalPole",
											"Exile_Item_JunkMetal",
											"Exile_Item_JunkMetal",
											"Exile_Item_JunkMetal",
											"Exile_Item_WoodPlank",
											"Exile_Item_WoodPlank",
											"Exile_Item_WoodPlank",
											"Exile_Item_WoodPlank"
										];
	DMS_BoxTools =						[
											"Exile_Item_Grinder",
											"Exile_Item_Handsaw",
											"Exile_Item_CanOpener",
											"Exile_Item_Pliers",
											"Exile_Item_Screwdriver",
											"Exile_Item_Rope",
											"Exile_Item_Foolbox"
										];
	DMS_BoxBuildingSupplies	=			[							// List of building supplies that can spawn in a crate ("DMS_BoxBaseParts", "DMS_BoxCraftingMaterials", and "DMS_BoxTools" are automatically added to this list. "DMS_BoxCraftingMaterials" is added twice for weight.)
											"Exile_Item_DuctTape",
											"Exile_Item_PortableGeneratorKit"
										] + DMS_BoxBaseParts + DMS_BoxCraftingMaterials + DMS_BoxCraftingMaterials + DMS_BoxTools;
	DMS_BoxOptics =						[							// List of optics that can spawn in a crate
											"optic_Arco",
											"optic_Hamr",
											"optic_Aco",
											"optic_Holosight",
											"optic_MRCO",
											"optic_SOS",
											"optic_DMS",
											"optic_LRPS",
											"optic_Nightstalker"			// Nightstalker scope lost thermal in Exile v0.9.4
										];
	DMS_BoxBackpacks =					[							//List of backpacks that can spawn in a crate
											"B_Bergen_rgr",
											"B_Carryall_oli",
											"B_Kitbag_mcamo",
											"B_Carryall_cbr",
											"B_FieldPack_oucamo",
											"B_FieldPack_cbr",
											"B_Bergen_blk"
										];
	DMS_BoxItems						= DMS_BoxSurvivalSupplies+DMS_BoxBuildingSupplies+DMS_BoxOptics;	// Random "items" can spawn optics, survival supplies, or building supplies

	DMS_Box_BreachingCharges =			[							// List of breaching charges (weighted). Not used (yet).
											"BreachingChargeBigMomma",
											"BreachingChargeMetal",
											"BreachingChargeMetal",
											"BreachingChargeWood",
											"BreachingChargeWood",
											"BreachingChargeWood"
										];

	DMS_RareLoot						= true;						// Potential chance to spawn rare loot in any crate.
	DMS_RareLootAmount					= 2;						// How many rare loot items to add.
	DMS_RareLootList =					[							// List of rare loot to spawn
											"Exile_Item_SafeKit",
											"Exile_Item_CodeLock"
										];
	DMS_RareLootChance					= 50;						// Percentage Chance to spawn rare loot in any crate | Default: 10%

	// Vehicles
	DMS_ArmedVehicles =					[							// List of armed vehicles that can spawn
											#ifdef USE_APEX_VEHICLES
											"B_T_LSV_01_armed_F",
											"O_T_LSV_02_armed_F",
											#endif
											"CUP_BAF_Jackal2_GMG_W",
											"CUP_BAF_Jackal2_L2A1_W",
											"CUP_B_BAF_Coyote_L2A1_W",
											"CUP_B_Dingo_GER_Wdl",
											"CUP_B_FV432_Bulldog_GB_W_RWS",
											"CUP_B_HMMWV_AGS_GPK_ACR",
											"CUP_B_HMMWV_Crows_M2_USA",
											"CUP_B_HMMWV_Crows_MK19_USA",
											"CUP_B_HMMWV_DSHKM_GPK_ACR",
											"CUP_B_HMMWV_M1114_USMC",
											"CUP_B_HMMWV_M2_GPK_USA",
											"CUP_B_HMMWV_M2_USA",
											"CUP_B_HMMWV_M2_USMC",
											"CUP_B_HMMWV_MK19_USMC",
											"CUP_B_HMMWV_SOV_USA",
											"CUP_B_HMMWV_Avenger_USMC",
											"CUP_B_HMMWV_Avenger_USA",
											"CUP_B_HMMWV_Avenger_NATO_T",
											"CUP_B_LAV25M240_USMC",
											"CUP_B_LAV25_HQ_USMC",
											"CUP_B_LAV25_USMC",
											"CUP_B_LR_MG_CZ_W",
											"CUP_B_LR_MG_GB_W",
											"CUP_B_LR_Special_CZ_W",
											"CUP_B_Mastiff_HMG_GB_W",
											"CUP_B_Ridgback_HMG_GB_W",
											"CUP_I_Datsun_PK_Random",
											"CUP_I_SUV_Armored_ION",
											"CUP_I_UAZ_MG_UN",
											"CUP_O_BRDM2_CHDKZ",
											"CUP_O_BRDM2_HQ_CHDKZ",
											"CUP_O_BRDM2_HQ_SLA",
											"CUP_O_BRDM2_SLA",
											"CUP_O_BTR90_HQ_RU",
											"CUP_O_BTR90_RU",
											"CUP_O_Datsun_PK_Random",
											"CUP_O_GAZ_Vodnik_PK_RU",
											"CUP_O_LR_MG_TKA",
											"CUP_O_LR_MG_TKM",
											"CUP_O_UAZ_AGS30_RU",
											"CUP_O_UAZ_MG_RU"
										];

	DMS_MilitaryVehicles =				[							// List of (unarmed) military vehicles that can spawn
											#ifdef USE_APEX_VEHICLES
											"B_T_LSV_01_unarmed_F",
											"O_T_LSV_02_unarmed_F",
											#endif
											"Exile_Car_Strider",
											"Exile_Car_Hunter",
											"Exile_Car_Ifrit",
											"CUP_B_HMMWV_Transport_USA",
											"CUP_B_HMMWV_Unarmed_USA",
											"CUP_B_HMMWV_Unarmed_USMC",
											"CUP_C_UAZ_Open_TK_CIV",
											"CUP_I_BTR40_TKG",
											"CUP_O_UAZ_Open_RU",
											"CUP_O_UAZ_Unarmed_RU"
										];

	DMS_TransportTrucks =				[							// List of transport trucks that can spawn
											"Exile_Car_Van_Guerilla01",
											"Exile_Car_Zamak",
											"Exile_Car_Tempest",
											"Exile_Car_HEMMT",
											"Exile_Car_Ural_Open_Military",
											"Exile_Car_Ural_Covered_Military",
											"CUP_B_LR_Transport_CZ_W",
											"CUP_B_LR_Transport_GB_W",
											"CUP_O_Ural_Empty_RU",
											"CUP_O_Ural_Open_RU",
											"CUP_O_Ural_RU",
											"CUP_O_Ural_Repair_RU"
										];

	DMS_RefuelTrucks =					[							// List of refuel trucks that can spawn
											"Exile_Car_Van_Fuel_Black",
											"Exile_Car_Van_Fuel_White",
											"Exile_Car_Van_Fuel_Red",
											"Exile_Car_Van_Fuel_Guerilla01",
											"Exile_Car_Van_Fuel_Guerilla02",
											"Exile_Car_Van_Fuel_Guerilla03",
											"CUP_O_Ural_Refuel_RU",
											"RHS_Ural_Flat_MSV_01",
											"RHS_Ural_Flat_VDV_01",
											"RHS_Ural_Flat_VMF_01",
											"RHS_Ural_Flat_VV_01",
											"RHS_Ural_Fuel_MSV_01",
											"RHS_Ural_Fuel_VDV_01",
											"RHS_Ural_Fuel_VMF_01",
											"RHS_Ural_Fuel_VV_01"
										];

	DMS_CivilianVehicles =				[							// List of civilian vehicles that can spawn
											#ifdef USE_APEX_VEHICLES
											#endif
											"Exile_Car_SUV_Red",
											"Exile_Car_Hatchback_Rusty1",
											"Exile_Car_Hatchback_Rusty2",
											"Exile_Car_Hatchback_Sport_Red",
											"Exile_Car_SUV_Red",
											"Exile_Car_Offroad_Rusty2",
											"Exile_Bike_QuadBike_Fia",
											"Fox_ChallengerBR",
											"Fox_ChallengerDev",
											"Fox_ChallengerDev2",
											"Fox_ChallengerYB",
											"Fox_ChallengerO",
											"Fox_ChallengerW",
											"Fox_DaytonaStratos",
											"Fox_CobraR_police",
											"Fox_Outsider",
											"Fox_GNX",
											"Fox_Charger_Apocalypse",
											"Fox_Viper",
											"Fox_F40",
											"Fox_Patrol",
											"Fox_Interceptor",
											"Fox_DaytonaGeneral",
											"Fox_Daytona",
											"Fox_Patrol2",
											"Fox_Patrol3",
											"Fox_BUS",
											"Fox_Megabus",
											"Fox_Fireengine",
											"Fox_LandCruiser",
											"Fox_Tahoe_Apocalypse",
											"Fox_Pickup_Apocalypse",
											"Fox_Tahoe_Apocalypse",
											"Fox_Landrover",
											"Fox_Landrover2",
											"Fox_Pickup",
											"Fox_Pickup_Tow",
											"Fox_Pickup6S",
											"Fox_LandCruiserFox",
											"Fox_LandCruiser3",
											"Fox_LandCruiser2",
											"Fox_Truck",
											"HAP_IF_blue_1",
											"HAP_IF_green_1",
											"HAP_IF_orange_1",
											"HAP_IF_pink_1",
											"HAP_IF_pirate_1",
											"HAP_IF_purple_1",
											"HAP_IF_red_1",
											"HAP_IF_snake_1",
											"HAP_IF_soa_black_1",
											"HAP_IF_winter_1",
											"HAP_IF_woodland_1",
											"HAP_MRAP_HUNTRS_1",
											"HAP_MRAP_blue_1",
											"HAP_MRAP_bomber_1",
											"HAP_MRAP_green_1",
											"HAP_MRAP_hobo_1",
											"HAP_MRAP_orange_1",
											"HAP_MRAP_red_1",
											"HAP_MRAP_snake_1",
											"HAP_MRAP_winter_1",
											"HAP_STRY_HUNTR_1",
											"HAP_STRY_desert1",
											"HAP_STRY_green1",
											"HAP_STRY_urban1",
											"HAP_STRY_woodland1",
											"HAP_ATV_new_black",
											"HAP_ATV_new_blue",
											"HAP_ATV_new_green",
											"HAP_ATV_new_orange",
											"HAP_ATV_new_yellow",
											"HAP_ATV_old_blue",
											"HAP_ATV_old_red",
											"HAP_ATV_old_teal",
											"HAP_OFFRD_HUNTR_1",
											"HAP_OFFRD_HUNTR_2",
											"HAP_OFFRD_autumn",
											"HAP_OFFRD_bandit",
											"HAP_OFFRD_banhammer",
											"HAP_OFFRD_digi_BW",
											"HAP_OFFRD_digi_red",
											"HAP_OFFRD_exilelogo",
											"HAP_OFFRD_generallee",
											"HAP_OFFRD_hemi",
											"HAP_OFFRD_hobo1",
											"HAP_OFFRD_kitty",
											"HAP_OFFRD_larry",
											"HAP_OFFRD_police1",
											"HAP_OFFRD_racer_BW",
											"HAP_OFFRD_racer_RW",
											"HAP_OFFRD_rustbucket1",
											"HAP_OFFRD_rustbucket2",
											"HAP_OFFRD_rustbucket3",
											"HAP_OFFRD_rustbucket_racer1",
											"HAP_OFFRD_woodland",
											"HAP_SUV_blue",
											"HAP_SUV_bmw1",
											"HAP_SUV_cop_black",
											"HAP_SUV_cop_blue",
											"HAP_SUV_medic",
											"HAP_SUV_red",
											"FMP_Hunter_Alpha",
											"FMP_Hunter_Bgcamo",
											"FMP_Hunter_Grunge",
											"FMP_Hunter_Milsim",
											"FMP_Hunter_Ocamo",
											"FMP_Hunter_Savage",
											"FMP_Hunter_Skull",
											"FMP_Hunter_Suba",
											"FMP_Strider_Carbon",
											"FMP_Strider_Hex",
											"FMP_Strider_Multi",
											"FMP_Strider_Radioactive",
											"FMP_Strider_Rusty",
											"FMP_Strider_Splatter",
											"FMP_Strider_Standard",
											"bv_458_aqua",
											"bv_458_baby_blue",
											"bv_458_baby_pink",
											"bv_458_black2",
											"bv_458_burgundy",
											"bv_458_cardinal",
											"bv_458_dark_green",
											"bv_458_gold",
											"bv_458_green",
											"bv_458_grey",
											"bv_458_lavender",
											"bv_458_light_blue",
											"bv_458_light_yellow",
											"bv_458_lime",
											"bv_458_marina_blue",
											"bv_458_navy_blue",
											"bv_458_orange",
											"bv_458_pink",
											"bv_458_purple",
											"bv_458_red",
											"bv_458_sand",
											"bv_458_silver",
											"bv_458_skin_blue",
											"bv_458_skin_camo",
											"bv_458_skin_camo_urban",
											"bv_ben_dover_aqua",
											"bv_ben_dover_baby_blue",
											"bv_ben_dover_baby_pink",
											"bv_ben_dover_black2",
											"bv_ben_dover_burgundy",
											"bv_ben_dover_cardinal",
											"bv_ben_dover_dark_green",
											"bv_ben_dover_gold",
											"bv_ben_dover_green",
											"bv_ben_dover_grey",
											"bv_ben_dover_lavender",
											"bv_ben_dover_light_blue",
											"bv_ben_dover_light_yellow",
											"bv_ben_dover_lime",
											"bv_ben_dover_marina_blue",
											"bv_ben_dover_navy_blue",
											"bv_ben_dover_orange",
											"bv_ben_dover_pink",
											"bv_ben_dover_purple",
											"bv_ben_dover_red",
											"bv_ben_dover_sand",
											"bv_ben_dover_silver",
											"bv_ben_dover_skin_blue",
											"bv_ben_dover_skin_camo",
											"bv_ben_dover_skin_camo_urban",
											"bv_caressa_gt_aqua",
											"bv_caressa_gt_baby_blue",
											"bv_caressa_gt_baby_pink",
											"bv_caressa_gt_black2",
											"bv_caressa_gt_burgundy",
											"bv_caressa_gt_cardinal",
											"bv_caressa_gt_dark_green",
											"bv_caressa_gt_gold",
											"bv_caressa_gt_green",
											"bv_caressa_gt_grey",
											"bv_caressa_gt_lavender",
											"bv_caressa_gt_light_blue",
											"bv_caressa_gt_light_yellow",
											"bv_caressa_gt_lime",
											"bv_caressa_gt_marina_blue",
											"bv_caressa_gt_navy_blue",
											"bv_caressa_gt_orange",
											"bv_caressa_gt_pink",
											"bv_caressa_gt_purple",
											"bv_caressa_gt_red",
											"bv_caressa_gt_sand",
											"bv_caressa_gt_silver",
											"bv_caressa_gt_skin_blue",
											"bv_caressa_gt_skin_camo",
											"bv_caressa_gt_skin_camo_urban",
											"bv_e60_m5_aqua",
											"bv_e60_m5_baby_blue",
											"bv_e60_m5_baby_pink",
											"bv_e60_m5_black2",
											"bv_e60_m5_burgundy",
											"bv_e60_m5_cardinal",
											"bv_e60_m5_dark_green",
											"bv_e60_m5_gold",
											"bv_e60_m5_green",
											"bv_e60_m5_grey",
											"bv_e60_m5_lavender",
											"bv_e60_m5_light_blue",
											"bv_e60_m5_light_yellow",
											"bv_e60_m5_lime",
											"bv_e60_m5_marina_blue",
											"bv_e60_m5_navy_blue",
											"bv_e60_m5_orange",
											"bv_e60_m5_pink",
											"bv_e60_m5_purple",
											"bv_e60_m5_red",
											"bv_e60_m5_sand",
											"bv_e60_m5_silver",
											"bv_e60_m5_skin_blue",
											"bv_e60_m5_skin_camo",
											"bv_e60_m5_skin_camo_urban",
											"bv_e63_amg_aqua",
											"bv_e63_amg_baby_blue",
											"bv_e63_amg_baby_pink",
											"bv_e63_amg_black2",
											"bv_e63_amg_burgundy",
											"bv_e63_amg_cardinal",
											"bv_e63_amg_dark_green",
											"bv_e63_amg_gold",
											"bv_e63_amg_green",
											"bv_e63_amg_grey",
											"bv_e63_amg_lavender",
											"bv_e63_amg_light_blue",
											"bv_e63_amg_light_yellow",
											"bv_e63_amg_lime",
											"bv_e63_amg_marina_blue",
											"bv_e63_amg_navy_blue",
											"bv_e63_amg_orange",
											"bv_e63_amg_pink",
											"bv_e63_amg_purple",
											"bv_e63_amg_red",
											"bv_e63_amg_sand",
											"bv_e63_amg_silver",
											"bv_e63_amg_skin_blue",
											"bv_e63_amg_skin_camo",
											"bv_e63_amg_skin_camo_urban",
											"bv_gtr_spec_V_aqua",
											"bv_gtr_spec_V_baby_blue",
											"bv_gtr_spec_V_baby_pink",
											"bv_gtr_spec_V_black2",
											"bv_gtr_spec_V_burgundy",
											"bv_gtr_spec_V_cardinal",
											"bv_gtr_spec_V_dark_green",
											"bv_gtr_spec_V_gold",
											"bv_gtr_spec_V_green",
											"bv_gtr_spec_V_grey",
											"bv_gtr_spec_V_lavender",
											"bv_gtr_spec_V_light_blue",
											"bv_gtr_spec_V_light_yellow",
											"bv_gtr_spec_V_lime",
											"bv_gtr_spec_V_marina_blue",
											"bv_gtr_spec_V_navy_blue",
											"bv_gtr_spec_V_orange",
											"bv_gtr_spec_V_pink",
											"bv_gtr_spec_V_purple",
											"bv_gtr_spec_V_red",
											"bv_gtr_spec_V_sand",
											"bv_gtr_spec_V_silver",
											"bv_gtr_spec_V_skin_blue",
											"bv_gtr_spec_V_skin_camo",
											"bv_gtr_spec_V_skin_camo_urban",
											"bv_shelly_bf_skin",
											"bv_shelly_bw_skin",
											"bv_shelly_rw_skin",
											"bv_shelly_wb_skin",
											"bv_shelly_wbl_skin",
											"bv_shelly_wf_skin",
											"bv_the_crowner_aqua",
											"bv_the_crowner_baby_blue",
											"bv_the_crowner_baby_pink",
											"bv_the_crowner_black2",
											"bv_the_crowner_burgundy",
											"bv_the_crowner_cardinal",
											"bv_the_crowner_dark_green",
											"bv_the_crowner_gold",
											"bv_the_crowner_green",
											"bv_the_crowner_grey",
											"bv_the_crowner_lavender",
											"bv_the_crowner_light_blue",
											"bv_the_crowner_light_yellow",
											"bv_the_crowner_lime",
											"bv_the_crowner_marina_blue",
											"bv_the_crowner_navy_blue",
											"bv_the_crowner_orange",
											"bv_the_crowner_pink",
											"bv_the_crowner_purple",
											"bv_the_crowner_red",
											"bv_the_crowner_sand",
											"bv_the_crowner_silver",
											"bv_the_crowner_skin_blue",
											"bv_the_crowner_skin_camo",
											"bv_the_crowner_skin_camo_urban",
											"bv_the_crowner_skin_white",
											"bv_monster_aqua",
											"bv_monster_baby_blue",
											"bv_monster_baby_pink",
											"bv_monster_black2",
											"bv_monster_burgundy",
											"bv_monster_cardinal",
											"bv_monster_dark_green",
											"bv_monster_gold",
											"bv_monster_green",
											"bv_monster_grey",
											"bv_monster_lavender",
											"bv_monster_light_blue",
											"bv_monster_light_yellow",
											"bv_monster_lime",
											"bv_monster_marina_blue",
											"bv_monster_navy_blue",
											"bv_monster_orange",
											"bv_monster_pink",
											"bv_monster_purple",
											"bv_monster_red",
											"bv_monster_sand",
											"bv_monster_silver",
											"bv_monster_skin_blue",
											"bv_monster_skin_camo",
											"bv_monster_skin_camo_urban",
											"CUP_B_HMMWV_Transport_USA",
											"CUP_B_HMMWV_Unarmed_USA",
											"CUP_B_HMMWV_Unarmed_USMC",
											"CUP_B_LR_Transport_CZ_W",
											"CUP_B_LR_Transport_GB_W",
											"CUP_C_UAZ_Open_TK_CIV",
											"CUP_I_BTR40_TKG",
											"CUP_O_UAZ_Open_RU",
											"CUP_O_UAZ_Unarmed_RU",
											"CUP_O_Ural_Empty_RU",
											"CUP_O_Ural_Open_RU",
											"CUP_O_Ural_RU",
											"CUP_O_Ural_Refuel_RU",
											"CUP_O_Ural_Repair_RU",
											"CUP_BAF_Jackal2_GMG_W",
											"CUP_BAF_Jackal2_L2A1_W",
											"CUP_B_BAF_Coyote_L2A1_W",
											"CUP_B_Dingo_GER_Wdl",
											"CUP_B_FV432_Bulldog_GB_W_RWS",
											"CUP_B_HMMWV_AGS_GPK_ACR",
											"CUP_B_HMMWV_Crows_M2_USA",
											"CUP_B_HMMWV_Crows_MK19_USA",
											"CUP_B_HMMWV_DSHKM_GPK_ACR",
											"CUP_B_HMMWV_M1114_USMC",
											"CUP_B_HMMWV_M2_GPK_USA",
											"CUP_B_HMMWV_M2_USA",
											"CUP_B_HMMWV_M2_USMC",
											"CUP_B_HMMWV_MK19_USMC",
											"CUP_B_HMMWV_SOV_USA",
											"CUP_B_HMMWV_Avenger_USMC",
											"CUP_B_HMMWV_Avenger_USA",
											"CUP_B_HMMWV_Avenger_NATO_T",
											"CUP_B_LAV25M240_USMC",
											"CUP_B_LAV25_HQ_USMC",
											"CUP_B_LAV25_USMC",
											"CUP_B_LR_MG_CZ_W",
											"CUP_B_LR_MG_GB_W",
											"CUP_B_LR_Special_CZ_W",
											"CUP_B_Mastiff_HMG_GB_W",
											"CUP_B_Ridgback_HMG_GB_W",
											"CUP_I_Datsun_PK_Random",
											"CUP_I_SUV_Armored_ION",
											"CUP_I_UAZ_MG_UN",
											"CUP_O_BRDM2_CHDKZ",
											"CUP_O_BRDM2_HQ_CHDKZ",
											"CUP_O_BRDM2_HQ_SLA",
											"CUP_O_BRDM2_SLA",
											"CUP_O_BTR90_HQ_RU",
											"CUP_O_BTR90_RU",
											"CUP_O_Datsun_PK_Random",
											"CUP_O_GAZ_Vodnik_PK_RU",
											"CUP_O_LR_MG_TKA",
											"CUP_O_LR_MG_TKM",
											"CUP_O_UAZ_AGS30_RU",
											"CUP_O_UAZ_MG_RU",
											"CUP_B_AH1Z_NoWeapons"
										];

	DMS_TransportHelis =				[							// List of transport helis that can spawn
											#ifdef USE_APEX_VEHICLES
											"B_T_VTOL_01_infantry_F",
											"O_T_VTOL_02_infantry_F",
											#endif
											"Exile_Chopper_Hummingbird_Green",
											"Exile_Chopper_Orca_BlackCustom",
											"Exile_Chopper_Mohawk_FIA",
											"Exile_Chopper_Huron_Black",
											"Exile_Chopper_Hellcat_Green",
											"Exile_Chopper_Taru_Transport_Black",
											"CUP_I_UH60L_Unarmed_FFV_Racs",
											"CUP_I_UH60L_Unarmed_RACS"
										];

	DMS_ReinforcementHelis =			[							// List of helis that can spawn for AI paratrooper reinforcements.
											//"B_Heli_Transport_01_camo_F"		// Ghosthawk: You'll have to whitelist this in infistar if you want to use it.
										] + DMS_TransportHelis;

	DMS_CarThievesVehicles =			[							// List of vehicles that can spawn in the "car thieves" mission. By default, it's just "DMS_MilitaryVehicles" and "DMS_TransportTrucks".
											//"Exile_Car_Offroad_Armed_Guerilla01"
										] + DMS_MilitaryVehicles + DMS_TransportTrucks;
/* Loot Settings */


DMS_ConfigLoaded = true;