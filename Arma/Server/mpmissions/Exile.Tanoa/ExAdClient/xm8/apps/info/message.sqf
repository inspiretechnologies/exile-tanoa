<t align='center' color='#E48A36'>GENERAL RULES</t><br />

<t align='center' color='#FFFF00'>Talk shit in side chat, get banned. If you have something to say, use the proper channels, contact an admin on our discord.</t><br />
<t align='center' color='#FFFFFF'>This is a PvPvE server, that means PvP is allowed and there is AI on missions and roaming in land vehicles, air, and boats.</t><br />
<t align='center' color='#FFFF00'>Although this is PvP, we discourage kill-on-sight. Be tactical about it, have a reason for the kill!</t><br />
<t align='center' color='#FFFFFF'>Joining the discord is not mandatory but highly recommended as you probably won't get admin help on side chat.</t><br />
<t align='center' color='#FFFF00'>No stealing in safezone with players present.</t><br />
<t align='center' color='#FFFFFF'>You can't claim missions here. However the only ones that will announce your presence is ZCP cap points.</t><br />
<t align='center' color='#FFFF00'>All rules may change without notice!</t><br />
<t align='center' color='#FFFFFF'></t><br />
<t align='center' color='#FFFFFF'>Discord: http://exilefiles.com/discord</t><br />

______________________________________________________________________________<br />

<br /><t color='#E48A36'>The AI</t><br />
<br /><t color='#FFFFFF'> There are roaming AI on.</t><br />
<br /><t color='#FFFF00'> AI in orange jumpsuits are on your side unless you shoot them.</t><br />
<br /><t color='#FFFFFF'> AI on server are smarter, they will steal unlocked vehicles.</t><br />
<br /><t color='#FFFFFF'> DMS Missions may have minefields around them, so don't just roll up in your armor.</t><br />
<br /><t color='#FFFF00'> AI do have armed vehicles and RPG launchers.</t><br />
______________________________________________________________________________<br />

<t color='#E48A36'>TRADE-ZONE RULES</t><br />
<t color='#FFFF00'>Leaving vehicles inside traders over restart will cause them to unlock. Vehicles left after restarts are free for taking</t><br />
<t color='#FFFFFF'>Check the safezone for players before you take anything.</t><br />
______________________________________________________________________________<br />

<t color='#E48A36'>BUILDING RULES</t><br />
<t color='#FFFF00'>Dont block roads</t><br />
<t color='#FFFFFF'>1000 meters from safe zones</t><br />
<t color='#FFFF00'>Dont build on airport property</t><br />
<t color='#FFFFFF'>Dont build on military bases</t><br />
<t color='#FFFF00'>Failure to follow these rules will result in your base being removed</t><br />
______________________________________________________________________________<br />





