/* 
Created by Lunchbox for use with Exile mod for Arma 3.
Copyright 2016, Lunchbox, All rights reserved.
*/
	//diag_log format ["+++++++++ Territory Payment Announcements :: Inside announcepay.sqf"];
	_playerUID = getPlayerUID player;
        {
            _flag = _x;
            _buildRights = _flag getVariable ["ExileTerritoryBuildRights", []];
            if (_playerUID in _buildRights) then
            {
			_nextDueDate = _flag getVariable ["ExileTerritoryMaintenanceDue", [0, 0, 0, 0, 0]];
			_name = _flag getVariable ["ExileTerritoryName", ""];
			_dueDate = format 
			[
				"%2/%3/%1",
				_nextDueDate select 0,
				_nextDueDate select 1,
				_nextDueDate select 2
			];
			 ["TaskCreated",["",format["<t color='#E48A36'>%2</t> territory payment due: <t color='#E48A36'>%1</t>",_dueDate,_name]]] call BIS_fnc_showNotification;
			 //diag_log format ["+++++++++ Territory Payment Announcements :: Announced Payment Date of %3 For UID %1 and Territory %2",_playerUID,_name,_dueDate];
            };
        }
        forEach (allMissionObjects "Exile_Construction_Flag_Static");