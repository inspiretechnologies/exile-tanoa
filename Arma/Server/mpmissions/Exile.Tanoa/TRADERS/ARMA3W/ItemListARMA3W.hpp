	///////////////////////////////////////////////////////////////////////////////
	// Arma 3 Weapons & Gear
	///////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////
	// Dropped by AI or default things not in traders - to be sold only
	///////////////////////////////////////////////////////////////////////////////
	class FirstAidKit				{ quality = 1; price = 100; };
	class Medikit 					{ quality = 1; price = 100; };
	class MineDetector				{ quality = 1; price = 100; };
	class ToolKit					{ quality = 1; price = 100; };

	///////////////////////////////////////////////////////////////////////////////
	// Launchers
	///////////////////////////////////////////////////////////////////////////////
	class launch_B_Titan_F  		{ quality = 2; price = 2500; sellPrice = 750; };
	class launch_B_Titan_short_F  	{ quality = 2; price = 2500; sellPrice = 750; };
	class launch_I_Titan_F  		{ quality = 2; price = 2500; sellPrice = 750; };
	class launch_I_Titan_short_F  	{ quality = 2; price = 2500; sellPrice = 750; };
	class launch_NLAW_F  			{ quality = 2; price = 2500; sellPrice = 750; };
	class launch_O_Titan_F 			{ quality = 2; price = 2500; sellPrice = 750; };
	class launch_O_Titan_short_F  	{ quality = 2; price = 2500; sellPrice = 750; };
	class launch_RPG32_F   			{ quality = 2; price = 2500; sellPrice = 750; };
	class launch_Titan_F  			{ quality = 2; price = 2500; sellPrice = 750; };
	class launch_Titan_short_F 		{ quality = 2; price = 2500; sellPrice = 750; };

	///////////////////////////////////////////////////////////////////////////////
	// Launcher Ammo
	///////////////////////////////////////////////////////////////////////////////
	class NLAW_F					{ quality = 1; price = 250; };
	class RPG32_F					{ quality = 1; price = 250; };
	class RPG32_HE_F				{ quality = 1; price = 250; };
	class Titan_AA					{ quality = 1; price = 250; };
	class Titan_AP					{ quality = 1; price = 250; };
	class Titan_AT					{ quality = 1; price = 250; };

	///////////////////////////////////////////////////////////////////////////////
	// Arma 3 HMG - NOT LISTED IN TRADER
	//////////////////////////////////////////////////////////////////////////////

	class B_HMG_01_F  				{ quality = 3; price = 7500; }; 		// Mk30 HMG
	class O_HMG_01_F  				{ quality = 3; price = 7500; }; 		// Mk30 HMG
	class I_HMG_01_F	  			{ quality = 3; price = 7500; }; 		// Mk30 HMG
	class B_HMG_01_high_F  			{ quality = 3; price = 7500; }; 		// Mk30 HMG Raised
	class O_HMG_01_high_F	  		{ quality = 3; price = 7500; }; 		// Mk30 HMG Raised
	class I_HMG_01_high_F  			{ quality = 3; price = 7500; }; 		// Mk30 HMG Raised
	class B_HMG_01_A_F  			{ quality = 3; price = 7500; }; 		// Mk30A HMG
	class O_HMG_01_A_F  			{ quality = 3; price = 7500; }; 		// Mk30A HMG
	class I_HMG_01_A_F	  			{ quality = 3; price = 7500; }; 		// Mk30A HMG

	///////////////////////////////////////////////////////////////////////////////
	// Arma 3 GMG - NOT LISTED IN TRADER
	//////////////////////////////////////////////////////////////////////////////
	class B_GMG_01_F  				{ quality = 3; price = 7500; };		// Mk32 GMG
	class O_GMG_01_F  				{ quality = 3; price = 7500; };		// Mk32 GMG
	class I_GMG_01_F  				{ quality = 3; price = 7500; };		// Mk32 GMG
	class B_GMG_01_high_F  			{ quality = 3; price = 7500; };		// Mk32 GMG Raised
	class O_GMG_01_high_F  			{ quality = 3; price = 7500; };		// Mk32 GMG Raised
	class I_GMG_01_high_F  			{ quality = 3; price = 7500; };		// Mk32 GMG Raised
	class B_GMG_01_A_F  			{ quality = 3; price = 7500; };		// Mk32A GMG
	class O_GMG_01_A_F  			{ quality = 3; price = 7500; };		// Mk32A GMG
	class I_GMG_01_A_F  			{ quality = 3; price = 7500; };		// Mk32A GMG

	///////////////////////////////////////////////////////////////////////////////
	// Arma 3 Mortar - NOT LISTED IN TRADER
	//////////////////////////////////////////////////////////////////////////////
	class B_Mortar_01_F  			{ quality = 3; price = 10000; };		// Mk3 Mortar
	class O_Mortar_01_F  			{ quality = 3; price = 10000; };		// Mk3 Mortar
	class I_Mortar_01_F  			{ quality = 3; price = 10000; };		// Mk3 Mortar
	class I_G_Mortar_01_F  			{ quality = 3; price = 10000; };		// Mk3 Mortar
	class B_G_Mortar_01_F  			{ quality = 3; price = 10000; };		// Mk3 Mortar
	class O_G_Mortar_01_F  			{ quality = 3; price = 10000; };		// Mk3 Mortar

	///////////////////////////////////////////////////////////////////////////////
	// Arma 3 AA - NOT LISTED IN TRADER
	//////////////////////////////////////////////////////////////////////////////
	class B_static_AA_F	  			{ quality = 3; price = 10000; };		// Titan Launcher (AA) [NATO]
	class O_static_AA_F  			{ quality = 3; price = 10000; };		// Titan Launcher (AA) [CSAT]
	class I_static_AA_F   			{ quality = 3; price = 10000; };		// Titan Launcher (AA) [AAF]
	class B_static_AT_F   			{ quality = 3; price = 10000; };		// Titan Launcher (AT) [NATO]
	class O_static_AT_F  			{ quality = 3; price = 10000; };		// Titan Launcher (AT) [CSAT]
	class I_static_AT_F   			{ quality = 3; price = 10000; };		// Titan Launcher (AT) [AAF]

	///////////////////////////////////////////////////////////////////////////////
	// Arma 3 Designator - NOT LISTED IN TRADER
	//////////////////////////////////////////////////////////////////////////////
	class B_Static_Designator_01_F   { quality = 3; price = 1000; };
	class O_Static_Designator_02_F   { quality = 3; price = 1000; };