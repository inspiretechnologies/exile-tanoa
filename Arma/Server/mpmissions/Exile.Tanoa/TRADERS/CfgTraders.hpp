	/**
	 * Weapons, scopes, silencers, ammo
	 */
	class Exile_Trader_Armory
	{
		name = "ARMORY";
		showWeaponFilter = 1;
		categories[] =
		{
			"APEXAmmunition",
			"APEXAssaultRifles",
			"APEXBipodAttachments",
			"APEXLightMachineGuns",
			"APEXMuzzleAttachments",
			"APEXOpticAttachments",
			"APEXPistols",
			"APEXSniperRifles",
			"APEXSubMachineGuns",
			"Ammunition",
			"AssaultRifles",
			"BipodAttachments",
			"CUPAmmunition",
			"CUPAssaultRifles",
			"CUPLightMachineGuns",
			"CUPMuzzleAttachments",
			"CUPOpticAttachments",
			"CUPPistols",
			"CUPPointerAttachments",
			"CUPSniperRifles",
			"CUPSubMachineGuns",
			"NIAAssaultRifles",
			"NIALightMachineGuns",
			"NIASubMachineGuns",
			"NIASniperRifles",
			"NIAMiniGun",
			"NIAAmmunition",
			"NIAMuzzleAttachments",
			"NIAAttachments",
			"NIAOpticAttachments",
			"GREFAmmunition",
			"GREFWeapons",
			"HAPASSAULTRIFLES",
			"HAPSNIPERRIFLES",
			"LightMachineGuns",
			"MuzzleAttachments",
			"OpticAttachments",
			"Pistols",
			"PointerAttachments",
			"RHSAmmunition",
			"RHSAssaultRifles",
			"RHSBipodAttachments",
			"RHSForegripAttachments",
			"RHSLightMachineGuns",
			"RHSMuzzleAttachments",
			"RHSOpticAttachments",
			"RHSPistols",
			"RHSPointerAttachments",
			"RHSSniperRifles",
			"RHSSubMachineGuns",
			"SAFAmmunition",
			"SAFAttachments",
			"SAFWeapons",
			"Shotguns",
			"SniperRifles",
			"SubMachineGuns"
		};
	};

	/**
	 * Satchels, nades, UAVs, static MGs
	 */
	class Exile_Trader_SpecialOperations
	{
		name = "SPECIAL OPERATIONS";
		showWeaponFilter = 1; // for noob tubes
		categories[] =
		{
			"A3LauncherAmmo",
			"A3Launchers",
			"DLCRocketLauncherAmmo",
            "DLCRocketLaunchers",
			"CUPExplosive",
			"CUPLauncherAmmo",
			"CUPLaunchers",
			"Explosives",
			"Flares",
			"Navigation",
			"RHSExplosives",
			"RHSLauncherAmmo",
			"RHSLaunchers",
			"RHSStaticMG",
			"RHSUGLAmmo",
			"SAFMines",
			"Smokes",
			"StaticMGs",
			"UAVs"
		};
	};

	/**
	 * Uniforms, vests, helmets, backpacks
	 */
	class Exile_Trader_Equipment
	{
		name = "EQUIPMENT";
		showWeaponFilter = 0;
		categories[] =
		{
			"APEXBackpacks",
			"APEXHeadgear",
			"APEXUniforms",
			"APEXVests",
			"Backpacks",
			"CUPBackpacks",
			"CUPHeadgear",
			"CUPUniforms",
			"CUPVests",
			"FirstAid",
			"GREFHeadgear",
			"GREFUniforms",
			"GREFVests",
			"HAPBACKPACKS",
			"HAPHEADGEAR",
			"HAPUNIFORMS",
			"HAPVESTS",
			"Headgear",
			"Glasses",
			"RHSAccessories",
			"RHSBackpacks",
			"RHSHeadgear",
			"RHSUniforms",
			"RHSVests",
			"SAFBackpacks",
			"SAFHeadgear",
			"SAFUniforms",
			"SAFVests",
			"Tools",
			"Uniforms",
			"Vests"
		};
	};

	/**
	 * Cans, cans, cans
	 */
	class Exile_Trader_Food
	{
		name = "FAST FOOD";
		showWeaponFilter = 0;
		categories[] =
		{
			"Drinks",
			"Food",
			"NonVeganFood"
		};
	};

	/**
	 * Light bulbs, metal, etc.
	 */
	class Exile_Trader_Hardware
	{
		name = "HARDWARE";
		showWeaponFilter = 0;
		categories[] =
		{
			"Hardware",
			"ExtendedBaseMod"
		};
	};

	/**
	 * Sells cars and general vehicles
	 */
	class Exile_Trader_Vehicle
	{
		name = "VEHICLE";
		showWeaponFilter = 0;
		categories[] =
		{
			"Bikes",
			"CUPBikes",			
			"Cars",
			"Trucks",			
			"A3Armed",
			"A3Cars",
			"A3Trucks",
			"ApexCars",
			"FOXCars",
			"FOXTrucks",
			"FMPCars",
			"FMPTrucks",
			"HAPCARS",
			"HAPTRUCKS",
			"CUPArmed",
			"CUPUnarmed",
			"GREFArmed",
			"GREFUnarmed",
			"RHSVehicles",
			"SAFArmed",
			"SAFUnarmed",
			"A3Tanks",
			"DLCTanks"
		};
	};

	/**
	 * Sells choppers and planes
	 */
	class Exile_Trader_Aircraft
	{
		name = "AIRCRAFT";
		showWeaponFilter = 0;
		categories[] =
		{
			"Choppers",
			"A3Planes",
			"A3armedChoppers",
			"A3unarmedChoppers",
			"ApexChoppers",
			"ApexPlanes",
			"ApexVTOL",
			"FFAAChoppers",
			"FFAAPlanes",
			"FFAAJets",
			"CUPChoppers",
			"CUPPlanes",
			"GREFChoppers",
			"GREFPlanes",
			"Planes",
			"JetPlanes",
			"RHSChoppers",
			"RHSPlanes"
		};
	};

	/**
	 * Sells ships and boats
	 */
	class Exile_Trader_Boat
	{
		name = "BOAT";
		showWeaponFilter = 0;
		categories[] =
		{
			"Boats",
			"A3Boats",
			"ApexBoats",
			"FFAABoats",
			"RHSBoats",
			"CUPBoats"
		};
	};

	class Exile_Trader_Diving
	{
		name = "DIVERS";
		showWeaponFilter = 0;
		categories[] =
		{
			"Diving"
		};
	};

	/**
	 * Sells Community Items
	 */
	class Exile_Trader_CommunityCustoms
	{
		name = "COMMUNITY";
		showWeaponFilter = 0;
		categories[] =
		{
			"Community"
		};
	};
	class Exile_Trader_CommunityCustoms2
	{
		name = "COMMUNITY";
		showWeaponFilter = 0;
		categories[] =
		{
			"Community2"
		};
	};

	class Exile_Trader_CommunityCustoms3
	{
		name = "COMMUNITY";
		showWeaponFilter = 0;
		categories[] =
		{
			"Community3"
		};
	};

	class Exile_Trader_CommunityCustoms4
	{
		name = "COMMUNITY";
		showWeaponFilter = 0;
		categories[] =
		{
			"Community4"
		};
	};

	class Exile_Trader_CommunityCustoms5
	{
		name = "COMMUNITY";
		showWeaponFilter = 0;
		categories[] =
		{
			"Community5"
		};
	};

	class Exile_Trader_CommunityCustoms6
	{
		name = "COMMUNITY";
		showWeaponFilter = 0;
		categories[] =
		{
			"Community6"
		};
	};

	class Exile_Trader_CommunityCustoms7
	{
		name = "COMMUNITY";
		showWeaponFilter = 0;
		categories[] =
		{
			"Community7"
		};
	};

	class Exile_Trader_CommunityCustoms8
	{
		name = "COMMUNITY";
		showWeaponFilter = 0;
		categories[] =
		{
			"Community8"
		};
	};

	class Exile_Trader_CommunityCustoms9
	{
		name = "COMMUNITY";
		showWeaponFilter = 0;
		categories[] =
		{
			"Community9"
		};
	};

	class Exile_Trader_CommunityCustoms10
	{
		name = "COMMUNITY";
		showWeaponFilter = 0;
		categories[] =
		{
			"Community10"
		};
	};